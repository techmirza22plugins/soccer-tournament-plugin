<?php

/**
 * Fired during plugin activation
 *
 * @link       https://wppb.me/
 * @since      1.0.0
 *
 * @package    Custom_Ontosoccer
 * @subpackage Custom_Ontosoccer/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Custom_Ontosoccer
 * @subpackage Custom_Ontosoccer/includes
 * @author     Tech Mirza <talhamirza2@gmail.com>
 */
class Custom_Ontosoccer_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
