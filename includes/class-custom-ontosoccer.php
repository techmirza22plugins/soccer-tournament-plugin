<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://wppb.me/
 * @since      1.0.0
 *
 * @package    Custom_Ontosoccer
 * @subpackage Custom_Ontosoccer/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Custom_Ontosoccer
 * @subpackage Custom_Ontosoccer/includes
 * @author     Tech Mirza <talhamirza2@gmail.com>
 */
class Custom_Ontosoccer {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Custom_Ontosoccer_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {
		if ( defined( 'CUSTOM_ONTOSOCCER_VERSION' ) ) {
			$this->version = CUSTOM_ONTOSOCCER_VERSION;
		} else {
			$this->version = '1.0.0';
		}
		$this->plugin_name = 'custom-ontosoccer';

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Custom_Ontosoccer_Loader. Orchestrates the hooks of the plugin.
	 * - Custom_Ontosoccer_i18n. Defines internationalization functionality.
	 * - Custom_Ontosoccer_Admin. Defines all hooks for the admin area.
	 * - Custom_Ontosoccer_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-custom-ontosoccer-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-custom-ontosoccer-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-custom-ontosoccer-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-custom-ontosoccer-public.php';

		$this->loader = new Custom_Ontosoccer_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Custom_Ontosoccer_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Custom_Ontosoccer_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Custom_Ontosoccer_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

		$this->loader->add_action( 'admin_init', $plugin_admin, 'custom_ontosoccer_plugin_settings' );
		$this->loader->add_action( 'admin_menu', $plugin_admin, 'custom_ontosoccer_plugin_menu' );

		// Register Custom Post Type
		$this->loader->add_action( 'init', $plugin_admin, 'custom_ontosoccer_admin_init' );

		// Creating Meta Box for Custom Post Type
		$this->loader->add_action( 'add_meta_boxes_custom-match', $plugin_admin, 'custom_match_meta_box' );

		// Save Meta Box Details
		$this->loader->add_action( 'save_post_custom-match', $plugin_admin, 'custom_match_save_details', 10, 3 );

		$this->loader->add_action( 'admin_post_custom_ontosoccer_register', $plugin_admin, 'custom_ontosoccer_register' );
		$this->loader->add_action( 'admin_post_nopriv_custom_ontosoccer_register', $plugin_admin, 'custom_ontosoccer_register' );

		$this->loader->add_action( 'admin_post_custom_ontosoccer_validate_email', $plugin_admin, 'custom_ontosoccer_validate_email' );
		$this->loader->add_action( 'admin_post_nopriv_custom_ontosoccer_validate_email', $plugin_admin, 'custom_ontosoccer_validate_email' );
		
		$this->loader->add_action( 'admin_post_custom_ontosoccer_change_password', $plugin_admin, 'custom_ontosoccer_change_password' );
		$this->loader->add_action( 'admin_post_nopriv_custom_ontosoccer_change_password', $plugin_admin, 'custom_ontosoccer_change_password' );
		
		$this->loader->add_action( 'admin_post_custom_ontosoccer_login', $plugin_admin, 'custom_ontosoccer_login' );
		$this->loader->add_action( 'admin_post_nopriv_custom_ontosoccer_login', $plugin_admin, 'custom_ontosoccer_login' );
		
		$this->loader->add_action( 'admin_post_custom_ontosoccer_profile_edit', $plugin_admin, 'custom_ontosoccer_profile_edit' );
		$this->loader->add_action( 'admin_post_nopriv_custom_ontosoccer_profile_edit', $plugin_admin, 'custom_ontosoccer_profile_edit' );
		
		$this->loader->add_action( 'admin_post_custom_ontosoccer_forgot_password', $plugin_admin, 'custom_ontosoccer_forgot_password' );
		$this->loader->add_action( 'admin_post_nopriv_custom_ontosoccer_forgot_password', $plugin_admin, 'custom_ontosoccer_forgot_password' );
		
		$this->loader->add_action( 'admin_post_custom_ontosoccer_contact_us', $plugin_admin, 'custom_ontosoccer_contact_us' );
		$this->loader->add_action( 'admin_post_nopriv_custom_ontosoccer_contact_us', $plugin_admin, 'custom_ontosoccer_contact_us' );
		
		$this->loader->add_action( 'admin_post_delete_custom_ontosoccer_contact_us', $plugin_admin, 'delete_custom_ontosoccer_contact_us' );
		$this->loader->add_action( 'admin_post_nopriv_delete_custom_ontosoccer_contact_us', $plugin_admin, 'delete_custom_ontosoccer_contact_us' );

		$this->loader->add_action( 'admin_post_custom_ontosoccer_add_testimonial', $plugin_admin, 'custom_ontosoccer_add_testimonial' );
		$this->loader->add_action( 'admin_post_nopriv_custom_ontosoccer_add_testimonial', $plugin_admin, 'custom_ontosoccer_add_testimonial' );
		
		$this->loader->add_action( 'admin_post_update_custom_ontosoccer_testimonial', $plugin_admin, 'update_custom_ontosoccer_testimonial' );
		$this->loader->add_action( 'admin_post_nopriv_update_custom_ontosoccer_testimonial', $plugin_admin, 'update_custom_ontosoccer_testimonial' );
		
		$this->loader->add_action( 'admin_post_delete_custom_ontosoccer_testimonial', $plugin_admin, 'delete_custom_ontosoccer_testimonial' );
		$this->loader->add_action( 'admin_post_nopriv_delete_custom_ontosoccer_testimonial', $plugin_admin, 'delete_custom_ontosoccer_testimonial' );
		
		$this->loader->add_action( 'admin_post_delete_custom_ontosoccer_subscription', $plugin_admin, 'delete_custom_ontosoccer_subscription' );
		$this->loader->add_action( 'admin_post_nopriv_delete_custom_ontosoccer_subscription', $plugin_admin, 'delete_custom_ontosoccer_subscription' );

		$this->loader->add_action( 'admin_post_delete_custom_ontosoccer_prediction', $plugin_admin, 'delete_custom_ontosoccer_prediction' );
		$this->loader->add_action( 'admin_post_nopriv_delete_custom_ontosoccer_prediction', $plugin_admin, 'delete_custom_ontosoccer_prediction' );
		
		$this->loader->add_action( 'admin_post_custom_ontosoccer_validate_subscription', $plugin_admin, 'custom_ontosoccer_validate_subscription' );
		$this->loader->add_action( 'admin_post_nopriv_custom_ontosoccer_validate_subscription', $plugin_admin, 'custom_ontosoccer_validate_subscription' );
		
		$this->loader->add_action( 'admin_post_send_newsletter_subscription_email', $plugin_admin, 'send_newsletter_subscription_email' );
		$this->loader->add_action( 'admin_post_nopriv_send_newsletter_subscription_email', $plugin_admin, 'send_newsletter_subscription_email' );
		
		$this->loader->add_action( 'admin_post_send_user_email', $plugin_admin, 'send_user_email' );
		$this->loader->add_action( 'admin_post_nopriv_send_user_email', $plugin_admin, 'send_user_email' );
		
		$this->loader->add_action( 'admin_post_send_sms', $plugin_admin, 'send_sms' );
		$this->loader->add_action( 'admin_post_nopriv_send_sms', $plugin_admin, 'send_sms' );
		
		$this->loader->add_action( 'admin_post_send_pin_number_to_user', $plugin_admin, 'send_pin_number_to_user' );
		$this->loader->add_action( 'admin_post_nopriv_send_pin_number_to_user', $plugin_admin, 'send_pin_number_to_user' );

		$this->loader->add_action( 'wp_ajax_check_email_address', $plugin_admin, 'check_email_address' );
		$this->loader->add_action( 'wp_ajax_nopriv_check_email_address', $plugin_admin, 'check_email_address' );

		$this->loader->add_action( 'wp_ajax_check_twitter_username', $plugin_admin, 'check_twitter_username' );
		$this->loader->add_action( 'wp_ajax_nopriv_check_twitter_username', $plugin_admin, 'check_twitter_username' );

		$this->loader->add_action( 'wp_ajax_check_validation_code', $plugin_admin, 'check_validation_code' );
		$this->loader->add_action( 'wp_ajax_nopriv_check_validation_code', $plugin_admin, 'check_validation_code' );

		$this->loader->add_action( 'wp_ajax_check_validation_code_for_subscription', $plugin_admin, 'check_validation_code_for_subscription' );
		$this->loader->add_action( 'wp_ajax_nopriv_check_validation_code_for_subscription', $plugin_admin, 'check_validation_code_for_subscription' );

		$this->loader->add_action( 'wp_ajax_check_subscription_email', $plugin_admin, 'check_subscription_email' );
		$this->loader->add_action( 'wp_ajax_nopriv_check_subscription_email', $plugin_admin, 'check_subscription_email' );

		$this->loader->add_filter( 'bulk_actions-users', $plugin_admin, 'users_bulk_actions' );

		$this->loader->add_filter( 'handle_bulk_actions-users', $plugin_admin, 'users_bulk_action_handler', 10, 3 );

		$this->loader->add_action( 'admin_notices', $plugin_admin, 'users_bulk_actions_notices' );

		$this->loader->add_filter( 'manage_users_columns', $plugin_admin, 'manage_users_columns' );

		$this->loader->add_filter( 'manage_users_custom_column', $plugin_admin, 'manage_users_custom_column', 10, 3 );
		
		$this->loader->add_action( 'wp_ajax_count_file_contents', $plugin_admin, 'count_file_contents' );

		$this->loader->add_action( 'wp_ajax_import_matches_records', $plugin_admin, 'import_matches_records' );
		$this->loader->add_action( 'wp_ajax_import_pin_numbers_records', $plugin_admin, 'import_pin_numbers_records' );

		$this->loader->add_action( 'wp_ajax_submit_week_fixtures', $plugin_admin, 'submit_week_fixtures' );
		
		$this->loader->add_action( 'wp_mail_content_type', $plugin_admin, 'wpse27856_set_content_type' );

		$this->loader->add_filter( 'manage_custom-match_posts_columns', $plugin_admin, 'manage_custom_match_posts_columns' );

		$this->loader->add_action( 'manage_posts_custom_column', $plugin_admin, 'manage_custom_match_custom_column', 10, 2 );

		$this->loader->add_action( 'quick_edit_custom_box', $plugin_admin, 'custom_quick_edit_fields', 10, 2 );

		$this->loader->add_action( 'save_post', $plugin_admin, 'custom_quick_edit_save' );

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Custom_Ontosoccer_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

		$plugin_public->add_shortcodes();

		// $this->loader->add_action( 'wp_footer', $plugin_public, 'custom_wp_footer' );

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Custom_Ontosoccer_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
