(function( $ ) {

	function isEmail(email) {
		var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		return regex.test(email);
	}
	
	$(document).ready(function() {
		// Global Query
		if ($(document).find(".mobile-number").length > 0) {
			var input = document.querySelector(".mobile-number");
			var iti = window.intlTelInput(input, {
				utilsScript: "http://localhost/fiverrboxchaty/wp-content/plugins/custom-ontosoccer/public/js/utils.js"
			});
		}

		// Date Picker
		$('.datepicker').datepicker({
			todayBtn: "linked",
			clearBtn: true
		});

		// Registration Submit
		$(document).on("click", ".custom-ontosoccer-register-shortcode .btn-submit", function() {
			$(document).find(".text-muted").hide();
			var validation = true;
			$(document).find(".custom-ontosoccer-register-shortcode .form-control").each(function() {
				if ($(this).val()) {
					$(this).closest(".form-group").find(".text-muted").html("").hide();
				} else {
					$(this).closest(".form-group").find(".text-muted").html("This field is required.").show();
					validation = false;
				}
			});
			var email = $(document).find(".email-address");
			if (isEmail(email.val())) {
				email.closest(".form-group").find(".text-muted").hide();
			} else {
				email.closest(".form-group").find(".text-muted").html("Please enter valid email address.").show();
				validation = false;
			}
			if (iti.isValidNumber()) {
				$(document).find(".text-muted.mobile-number").hide();
			} else {
				$(document).find(".text-muted.mobile-number").html("Please enter valid mobile number.").show();
				var errorCode = iti.getValidationError();
				console.log(errorCode);
				validation = false;
			}
			if ( ! validation ) {
				return false;
			} else {
				var data = {
					'action': 'check_email_address',
					'email': email.val()
				};
				// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
				jQuery.post(my_ajax_object.ajax_url, data, function(response) {
					if (response > 0) {
						email.closest(".form-group").find(".text-muted").html("This email address is already in use. Please find another!").show();
						return false;
					} else {
						var data = {
							'action': 'check_twitter_username',
							'username': $(document).find("#twitter_username").val()
						};
						// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
						jQuery.post(my_ajax_object.ajax_url, data, function(response) {
							if (response > 0) {
								$(document).find(".registration-form").submit();
							} else {
								$(document).find("#twitter_username").closest(".form-group").find(".text-muted").html("Twitter username is not valid. Please try again!").show();
								return false;
							}
						});
					}
				});
			}
		});

		// Validation Email Submit
		$(document).on("click", ".custom-ontosoccer-validate-email-shortcode .btn-submit", function() {
			var validation = true;
			$(document).find(".custom-ontosoccer-validate-email-shortcode .form-control").each(function() {
				if ($(this).val()) {
					$(this).closest(".form-group").find(".text-muted").html("").hide();
				} else {
					$(this).closest(".form-group").find(".text-muted").html("This field is required.").show();
					validation = false;
				}
			});
			if ( ! validation ) {
				return false;
			} else {
				var data = {
					'action': 'check_validation_code',
					'user_id': $(document).find("input[name=user_id]").val(),
					'validation_code': $(document).find(".validation_code").val()
				};
				// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
				jQuery.post(my_ajax_object.ajax_url, data, function(response) {
					if (response > 0) {
						$(document).find(".validate-email-form").submit();
					} else {
						$(document).find(".validation_code").closest(".form-group").find(".text-muted").html("This validation code seems not to be valid. Please try again.").show();
						return false;
					}
				});
			}
		});

		// Validation Password Submit
		$(document).on("click", ".custom-ontosoccer-change-password-shortcode .btn-submit", function() {
			var validation = true;
			$(document).find(".custom-ontosoccer-change-password-shortcode .form-control").each(function() {
				if ($(this).val()) {
					$(this).closest(".form-group").find(".text-muted").html("").hide();
				} else {
					$(this).closest(".form-group").find(".text-muted").html("This field is required.").show();
					validation = false;
				}
			});
			if ( ! validation ) {
				return false;
			} else {
				var password = $(document).find(".custom-ontosoccer-change-password-shortcode #password").val();
				var repeatPassword = $(document).find(".custom-ontosoccer-change-password-shortcode #repeat_password").val();
				if (password == repeatPassword) {
					$(document).find(".change-password-form").submit();
				} else {
					$(document).find(".custom-ontosoccer-change-password-shortcode #repeat_password").closest(".form-group").find(".text-muted").html("Both passwords should match. Please try again.").show();
					return false;
				}
			}
		});

		// Validation Login Submit
		$(document).on("click", ".custom-ontosoccer-login-shortcode .btn-submit", function() {
			var validation = true;
			$(document).find(".custom-ontosoccer-login-shortcode .form-control").each(function() {
				if ($(this).val()) {
					$(this).closest(".form-group").find(".text-muted").html("").hide();
				} else {
					$(this).closest(".form-group").find(".text-muted").html("This field is required.").show();
					validation = false;
				}
			});
			if ( ! validation ) {
				return false;
			} else {
				$(document).find(".login-form").submit();
			}
		});

		// Validation Profile Edit
		$(document).on("click", ".custom-ontosoccer-profile-edit-shortcode .btn-submit", function() {
			var validation = true;
			$(document).find(".custom-ontosoccer-login-shortcode .form-control").each(function() {
				if ($(this).val()) {
					$(this).closest(".form-group").find(".text-muted").html("").hide();
				} else {
					$(this).closest(".form-group").find(".text-muted").html("This field is required.").show();
					validation = false;
				}
			});
			if ( ! validation ) {
				return false;
			} else {
				$(document).find(".profile-edit-form").submit();
			}
		});

		// Validation Forgot Password
		$(document).on("click", ".custom-ontosoccer-forgot-password-shortcode .btn-submit", function() {
			var validation = true;
			$(document).find(".custom-ontosoccer-forgot-password-shortcode .form-control").each(function() {
				if ($(this).val()) {
					$(this).closest(".form-group").find(".text-muted").html("").hide();
				} else {
					$(this).closest(".form-group").find(".text-muted").html("This field is required.").show();
					validation = false;
				}
			});
			var email = $(document).find(".email-address");
			if (isEmail(email.val())) {
				email.closest(".form-group").find(".text-muted").hide();
			} else {
				email.closest(".form-group").find(".text-muted").html("Please enter valid email address.").show();
				validation = false;
			}
			if ( ! validation ) {
				return false;
			} else {
				$(document).find(".forgot-password-form").submit();
			}
		});

		// Contact Us Submit
		$(document).on("click", ".custom-ontosoccer-contact-us-shortcode .btn-submit", function() {
			var validation = true;
			$(document).find(".custom-ontosoccer-contact-us-shortcode .form-control").each(function() {
				if ($(this).val()) {
					$(this).closest(".form-group").find(".text-muted").html("").hide();
				} else {
					$(this).closest(".form-group").find(".text-muted").html("This field is required.").show();
					validation = false;
				}
			});
			var email = $(document).find(".email-address");
			if (isEmail(email.val())) {
				email.closest(".form-group").find(".text-muted").hide();
			} else {
				email.closest(".form-group").find(".text-muted").html("Please enter valid email address.").show();
				validation = false;
			}
			if (iti.isValidNumber()) {
				$(document).find(".text-muted.mobile-number").hide();
			} else {
				$(document).find(".text-muted.mobile-number").html("Please enter valid mobile number.").show();
				var errorCode = iti.getValidationError();
				console.log(errorCode);
				validation = false;
			}
			if ( ! validation ) {
				return false;
			} else {
				$(document).find(".contact-us-form").submit();
			}
		});

		// Add Testimonial Submit
		$(document).on("click", ".custom-ontosoccer-add-testimonial-shortcode .btn-submit", function() {
			var validation = true;
			$(document).find(".custom-ontosoccer-add-testimonial-shortcode .form-control").each(function() {
				if ($(this).val()) {
					$(this).closest(".form-group").find(".text-muted").html("").hide();
				} else {
					$(this).closest(".form-group").find(".text-muted").html("This field is required.").show();
					validation = false;
				}
			});
			var email = $(document).find(".email-address");
			if (isEmail(email.val())) {
				email.closest(".form-group").find(".text-muted").hide();
			} else {
				email.closest(".form-group").find(".text-muted").html("Please enter valid email address.").show();
				validation = false;
			}
			if (iti.isValidNumber()) {
				$(document).find(".text-muted.mobile-number").hide();
			} else {
				$(document).find(".text-muted.mobile-number").html("Please enter valid mobile number.").show();
				var errorCode = iti.getValidationError();
				console.log(errorCode);
				validation = false;
			}
			if ( ! validation ) {
				return false;
			} else {
				$(document).find(".add-testimonial-form").submit();
			}
		});

		// Subscription Newsletter
		$(document).on("click", ".subscription-form input[type=submit]", function(e) {
			e.preventDefault();
			var validation = true;
			var email = $(document).find(".subscription-form input[type=email]");
			if (isEmail(email.val())) {
				$(document).find(".subscription-alert.alert-danger").hide();
			} else {
				$(document).find(".subscription-alert.alert-danger").html("Please enter valid email address.").show();
				validation = false;
			}
			if ( ! validation ) {
				return false;
			} else {
				$(document).find(".subscription-alert").hide();
				var data = {
					'action': 'check_subscription_email',
					'email': email.val()
				};
				// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
				jQuery.post(my_ajax_object.ajax_url, data, function(response) {
					if (response > 0) {
						$(document).find(".subscription-alert.alert-success").html("We have sent you the confirmation email! Please see it in your inbox.").show();
					} else {
						$(document).find(".subscription-alert.alert-danger").html("This email address has been already in our subscription list. Please try another.").show();
						return false;
					}
				});
			}
		});

		// Validation Email for Subscription Submit
		$(document).on("click", ".custom-ontosoccer-validate-subscription-shortcode .btn-submit", function() {
			var validation = true;
			$(document).find(".custom-ontosoccer-validate-subscription-shortcode .form-control").each(function() {
				if ($(this).val()) {
					$(this).closest(".form-group").find(".text-muted").html("").hide();
				} else {
					$(this).closest(".form-group").find(".text-muted").html("This field is required.").show();
					validation = false;
				}
			});
			if ( ! validation ) {
				return false;
			} else {
				var data = {
					'action': 'check_validation_code_for_subscription',
					'validation_code': $(document).find(".validation_code").val()
				};
				// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
				jQuery.post(my_ajax_object.ajax_url, data, function(response) {
					if (response > 0) {
						$(document).find(".validate-subscription-form").submit();
					} else {
						$(document).find(".validation_code").closest(".form-group").find(".text-muted").html("This validation code seems not to be valid. Please try again.").show();
						return false;
					}
				});
			}
		});

		// On Submit Predictions
		$(document).on("click", ".submit-prediction-btn", function() {
			$(document).find(".custom-ontosoccer-week-fixtures-shortcode .alert.alert-danger").html("").hide();
			$(document).find(".custom-ontosoccer-week-fixtures-shortcode .alert.alert-success").html("").hide();
			var matchCount = 0;
			$(document).find(".custom-ontosoccer-week-fixtures-shortcode .match-entry").each(function() {
				if ($(this).find("td:eq(2) select").val() && $(this).find("td:eq(4) select").val()) {
					matchCount++;
				}
			});
			if (matchCount >= 3 && matchCount <= 6) {
				var matches = [];
				$(document).find(".custom-ontosoccer-week-fixtures-shortcode .match-entry").each(function() {
					if ($(this).find("td:eq(2) select").val() && $(this).find("td:eq(4) select").val()) {
						var match = {
							"ID": parseInt($(this).attr("data-matchID")),
							"firstTeamScore": parseInt($(this).find("td:eq(2) select").val()),
							"secondTeamScore": parseInt($(this).find("td:eq(4) select").val()),
						}
						matches.push(match);
					}
				});
				var data = {
					'action': 'submit_week_fixtures',
					'week_fixture': $(document).find(".week-fixutre").attr("data-week-fixture"),
					'matches': matches
				};
				// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
				jQuery.post(my_ajax_object.ajax_url, data, function(response) {
					$(document).find(".custom-ontosoccer-week-fixtures-shortcode .alert.alert-success").html("Successfully submitted the prediction against current week.").show();
					$(document).find(".custom-ontosoccer-week-fixtures-shortcode #predicting-matches").val(3);
					$(document).find(".custom-ontosoccer-week-fixtures-shortcode .match-entry").each(function() {
						$(this).find("td:eq(2) select").val("");
						$(this).find("td:eq(4) select").val("");
					});
				});
			} else {
				$(document).find(".custom-ontosoccer-week-fixtures-shortcode .alert.alert-danger").html("Minimum match prediction would be 3 and maximum match prediction would be 6.").show();
				return false;
			}
		});

		$(document).on("change", ".custom-ontosoccer-week-fixtures-standings-shortcode #week-fixture", function() {
			var pageURL = location.protocol + '//' + location.host + location.pathname;
			location.href = pageURL + "?week=" + $(this).val();
		});

	});

})( jQuery );
