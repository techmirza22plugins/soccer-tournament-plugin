<?php
    $args = array(
        'post_type'         => 'co-testimonial',
        'orderby'           => 'ID',
        'post_status'       => 'publish',
        'order'             => 'DESC',
        'posts_per_page'    => -1,
    );
    $posts = get_posts( $args );
?>

<style>
    blockquote {
        background-color: transparent;
        border-left-color: transparent;
    }
</style>
<div class="custom-ontosoccer-list-testimonials-shortcode">
    <div class="row justify-content-center">
        <div class="col-md-8 text-center">
            <h4 class="mb-3">Check what our Customers are Saying</h4>
            <h6 class="subtitle font-weight-normal">You can relay on our amazing features list and also our customer services will be great experience for you without doubt</h6>
        </div>
    </div>
    <div class="row moverspackers-services moverspackers-services-list mt-5">
        <?php foreach ($posts as $post) { ?>
            <div class="col-sm-12">
                <div class="moverspackers-services-wrap">
                    <div class="moverspackers-services-text">
                        <blockquote class="blockquote text-center">
                            <p class="mb-0" style="color: black;">“<?php echo get_post_meta( $post->ID, 'message', true ); ?>”</p>
                            <footer class="blockquote-footer mt-5"><?php echo get_post_meta( $post->ID, 'display_name', true ); ?></footer>
                        </blockquote>
                    </div>
                </div>
            </div>
        <?php } ?>
    </div>
</div>