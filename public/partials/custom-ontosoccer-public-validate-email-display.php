<?php
    if (isset($_GET['user']) && !empty($_GET['user'])) {
        $user_id = (int) $_GET['user'];
        $first_name = get_user_meta( $user_id, 'first_name', true );
        $last_name = get_user_meta( $user_id, 'last_name', true );
    }
?>
<?php if (isset($_GET['user']) && !empty($_GET['user'])) { ?>
    <div class="custom-ontosoccer-validate-email-shortcode">
        <div class="validate-email-form">
            <h2 class="moverspackers-widget-heading moverspackers-color mb-3">Hi, <?php echo $first_name; ?> <?php echo $last_name; ?>!</h2>
            <form action="<?php echo admin_url('admin-post.php'); ?>" class="validate-email-form" method="POST">
                <input type="hidden" name="action" value="custom_ontosoccer_validate_email">
                <input type="hidden" name="user_id" value="<?php echo $user_id ?>">
                <?php if (isset($_GET['password-success']) && !empty($_GET['password-success']) && $_GET['password-success'] == 'true') { ?>
                    <input type="hidden" name="verify_password" value="true">
                <?php } ?>
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="validation_code">Validation Code</label>
                            <input type="text" class="form-control validation_code" id="validation_code" name="validation_code">
                            <small class="form-text text-muted">We'll never share your email with anyone else.</small>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <button type="button" class="btn btn-primary btn-block btn-submit">Validate Code</button>
                        </div>
                    </div>
                </div>       
            </form>
            <div class="text-center">Already have an account? <a href="#">Login Here</a></div>
        </div>
    </div>
<?php } else { ?>
    <div class="custom-ontosoccer-validate-subscription-shortcode">
        <div class="validate-subscription-form">
            <h2 class="moverspackers-widget-heading moverspackers-color mb-3">Email Verification for Subscription!</h2>
            <?php if (isset($_GET['subscription-success']) && !empty($_GET['subscription-success']) && $_GET['subscription-success'] == 'true') : ?>
            <div class="alert alert-success alert-dismissible show" role="alert">
                <strong>Your email has been subscribed to our newsletter successfully!</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <?php endif; ?>
            <form action="<?php echo admin_url('admin-post.php'); ?>" class="validate-subscription-form" method="POST">
                <input type="hidden" name="action" value="custom_ontosoccer_validate_subscription">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="validation_code">Validation Code</label>
                            <input type="text" class="form-control validation_code" id="validation_code" name="validation_code">
                            <small class="form-text text-muted">We'll never share your email with anyone else.</small>
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                            <button type="button" class="btn btn-primary btn-block btn-submit">Validate Code</button>
                        </div>
                    </div>
                </div>       
            </form>
            <div class="text-center">Already have an account? <a href="#">Login Here</a></div>
        </div>
    </div>
<?php } ?>