<?php
    $user_id = (int) $_GET['user'];
    $first_name = get_user_meta( $user_id, 'first_name', true );
    $last_name = get_user_meta( $user_id, 'last_name', true );
?>
<div class="custom-ontosoccer-change-password-shortcode">
    <div class="change-password-form">
        <h2 class="moverspackers-widget-heading moverspackers-color mb-3">Hi, <?php echo $first_name; ?> <?php echo $last_name; ?>!</h2>
        <?php if (isset($_GET['change-password']) && !empty($_GET['change-password']) && $_GET['change-password'] == 'true') : ?>
        <div class="alert alert-success alert-dismissible show" role="alert">
            <strong>Validation done successfully!</strong> Please set password for your account.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif; ?>
        <?php if (isset($_GET['verify-password']) && !empty($_GET['verify-password']) && $_GET['verify-password'] == 'true') : ?>
        <div class="alert alert-success alert-dismissible show" role="alert">
            <strong>Verification done successfully!</strong> Please set new password for your account.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif; ?>
        <form action="<?php echo admin_url('admin-post.php'); ?>" class="change-password-form" method="POST">
            <input type="hidden" name="action" value="custom_ontosoccer_change_password">
            <input type="hidden" name="user_id" value="<?php echo $user_id; ?>">
            <?php if (isset($_GET['verify-password']) && !empty($_GET['verify-password']) && $_GET['verify-password'] == 'true') { ?>
                <input type="hidden" name="verify_password" value="true">
            <?php } ?>
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="password" name="password">
                        <small class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="repeat_password">
                            <?php if (isset($_GET['verify-password']) && !empty($_GET['verify-password']) && $_GET['verify-password'] == 'true') { ?>
                                New Password
                            <?php } else { ?>
                                Repeat Password
                            <?php } ?>
                        </label>
                        <input type="password" class="form-control" id="repeat_password" name="repeat_password">
                        <small class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <button type="button" class="btn btn-primary btn-block btn-submit">Submit Password</button>
                    </div>
                </div>
            </div>       
        </form>
        <div class="text-center">Don't have account? <a href="#">Register Here</a></div>
    </div>
</div>