<?php
    $user_id = get_current_user_id();
    $first_name = get_user_meta( $user_id, 'first_name', true );
    $last_name = get_user_meta( $user_id, 'last_name', true );
    $date_of_birth = get_user_meta( $user_id, 'date_of_birth', true );
	$mobile_number = get_user_meta( $user_id, 'mobile_number', true );
	$twitter_username = get_user_meta( $user_id, 'twitter_username', true );
?>
<div class="custom-ontosoccer-profile-edit-shortcode">
    <div class="signup-form">
        <h2 class="moverspackers-widget-heading moverspackers-color mb-3">Hi, <?php echo $first_name; ?> <?php echo $last_name; ?>!</h2>
        <p class="hint-text mb-3">You can edit information in the following fields.</p>
        <?php if (isset($_GET['edit-success']) && !empty($_GET['edit-success']) && $_GET['edit-success'] == 'true') : ?>
        <div class="alert alert-success alert-dismissible show" role="alert">
            <strong>Profile edited successfully!</strong>.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif; ?>
        <?php if (isset($_GET['edit-success']) && !empty($_GET['edit-success']) && $_GET['edit-success'] == 'false') : ?>
        <div class="alert alert-danger alert-dismissible show" role="alert">
            <strong>Oh Snap!</strong> Something wrong went to server.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif; ?>
        <form action="<?php echo admin_url('admin-post.php'); ?>" class="profile-edit-form" method="POST">
            <input type="hidden" name="action" value="custom_ontosoccer_profile_edit">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="first_name">First Name</label>
                        <input type="text" class="form-control" id="first_name" name="first_name" value="<?php echo $first_name; ?>">
                        <small class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="last_name">Last Name</label>
                        <input type="text" class="form-control" id="last_name" name="last_name" value="<?php echo $last_name; ?>">
                        <small class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="date_of_birth">Date of Birth</label>
                        <input type="text" class="form-control datepicker" id="date_of_birth" name="date_of_birth" value="<?php echo $date_of_birth; ?>">
                        <small class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="mobile_number">Mobile Number</label>
                        <input type="tel" class="form-control mobile-number" id="mobile_number" name="mobile_number" value="<?php echo $mobile_number; ?>">
                        <small class="form-text text-muted mobile-number pl-0">We'll never share your email with anyone else.</small>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="twitter_username">Twitter Username</label>
                        <input type="text" class="form-control" id="twitter_username" name="twitter_username" value="<?php echo $twitter_username; ?>">
                        <small class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <button type="button" class="btn btn-primary btn-block btn-submit">Edit Profile</button>
                    </div>
                </div>
            </div>       
        </form>
    </div>
</div>