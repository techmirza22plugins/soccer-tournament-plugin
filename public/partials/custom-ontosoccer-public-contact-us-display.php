<div class="custom-ontosoccer-contact-us-shortcode">
    <div class="contact-us-form">
        <h2 class="moverspackers-widget-heading moverspackers-color mb-3">Contact Us</h2>
        <?php if (isset($_GET['success']) && !empty($_GET['success']) && $_GET['success'] == 'true') : ?>
        <div class="alert alert-success alert-dismissible show" role="alert">
            <strong>Your query is successfully submitted!</strong> We will try to reply you soon.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif; ?>
        <?php if (isset($_GET['success']) && !empty($_GET['success']) && $_GET['success'] == 'false') : ?>
        <div class="alert alert-danger alert-dismissible show" role="alert">
            <strong>Oh Snap!</strong> Something wrong went to server.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif; ?>
        <form action="<?php echo admin_url('admin-post.php'); ?>" class="contact-us-form" method="POST">
            <input type="hidden" name="action" value="custom_ontosoccer_contact_us">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="first_name">First Name</label>
                        <input type="text" class="form-control" id="first_name" name="first_name">
                        <small class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="last_name">Last Name</label>
                        <input type="text" class="form-control" id="last_name" name="last_name">
                        <small class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="email_address">Email Address</label>
                        <input type="text" class="form-control email-address" id="email_address" name="email_address">
                        <small class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="mobile_number">Mobile Number</label>
                        <input type="tel" class="form-control mobile-number" id="mobile_number" name="mobile_number">
                        <small class="form-text text-muted mobile-number pl-0">We'll never share your email with anyone else.</small>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="message">Message</label>
                        <textarea name="message" id="message" class="form-control" cols="30" rows="10"></textarea>
                        <small class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <button type="button" class="btn btn-primary btn-block btn-submit">Message Now</button>
                    </div>
                </div>
            </div>       
        </form>
    </div>
</div>