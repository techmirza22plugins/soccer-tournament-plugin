<?php
    $customOntoSoccerSettings = get_option('custom_ontosoccer_settings', array());
    $custom_ontosoccer_registration_page_id = $customOntoSoccerSettings['custom_ontosoccer_registration_page_id'];
    $custom_ontosoccer_login_page_id = $customOntoSoccerSettings['custom_ontosoccer_login_page_id'];
    $custom_ontosoccer_forgot_password_page_id = $customOntoSoccerSettings['custom_ontosoccer_forgot_password_page_id'];
?>
<div class="custom-ontosoccer-login-shortcode">
    <div class="login-form">
        <h2 class="moverspackers-widget-heading moverspackers-color mb-3">Login</h2>
        <?php if (isset($_GET['account-validated']) && !empty($_GET['account-validated']) && $_GET['account-validated'] == 'true') : ?>
        <div class="alert alert-success alert-dismissible show" role="alert">
            <strong>Congractulation on Account Setup!</strong> Please login to your account.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif; ?>
        <?php if (isset($_GET['password-changed']) && !empty($_GET['password-changed']) && $_GET['password-changed'] == 'true') : ?>
        <div class="alert alert-success alert-dismissible show" role="alert">
            <strong>Congractulation!</strong> You can login with your new password.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif; ?>
        <?php if (isset($_GET['success']) && !empty($_GET['success']) && $_GET['success'] == 'false') : ?>
        <div class="alert alert-danger alert-dismissible show" role="alert">
            <strong>Wrong Email / Username and Password!</strong> Please try again.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif; ?>
        <?php if (isset($_GET['ban']) && !empty($_GET['ban']) && $_GET['ban'] == 'true') : ?>
        <div class="alert alert-danger alert-dismissible show" role="alert">
            <strong>Your account has been banned.</strong> Please contact admin.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif; ?>
        <form action="<?php echo admin_url('admin-post.php'); ?>" class="login-form" method="POST">
            <input type="hidden" name="action" value="custom_ontosoccer_login">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="email_address">Email / Username</label>
                        <input type="email" class="form-control" id="email_username" name="email_username">
                        <small class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" id="password" name="password">
                        <small class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <input type="submit" class="btn btn-primary btn-block btn-submit" value="Login">
                    </div>
                </div>
            </div>       
        </form>
        <div class="text-center">Forgot Password? <a href="<?php echo get_permalink($custom_ontosoccer_forgot_password_page_id); ?>">Click Here</a></div>
        <div class="text-center">Don't have account? <a href="#">Register Here</a></div>
    </div>
</div>