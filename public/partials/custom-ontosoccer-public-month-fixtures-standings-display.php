<?php
    $weekFixture = '' . date('Y/m/d');
    $weekFixture = new DateTime($weekFixture);
    $weekFixture = $weekFixture->format("W");
    if (isset($_GET['month']) && !empty($_GET['month'])) {
        $weekFixture = $_GET['month'];
    }
?>
<div class="custom-ontosoccer-week-fixtures-standings-shortcode">
    <h2 class="moverspackers-widget-heading moverspackers-color mb-3">Month Fixtures Winnings</h2>
    <label for="week-fixture" class="form-label"><strong>Select Week</strong></label>
    <select name="week_fixture" id="week-fixture" class="form-control">
        <option value="">Select one</option>
        <?php for ( $i = 1; $i <= 12; $i++ ) { ?>
            <option value="<?php echo $i; ?>" <?php echo ($i == $weekFixture) ? 'selected' : ''; ?>><?php echo $i; ?></option>
        <?php } ?>
    </select>
    <table class="table table-striped table-bordered table-hover mt-5">
        <thead>
            <tr>
                <th>#</th>
                <th>User</th>
                <th>Points</th>
                <th>Winnings</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $users = get_users();
                $count = 0;
                foreach ($users as $user) {
                    $args = array(
                        'post_type'         => 'custom-week-win',
                        'orderby'           => 'ID',
                        'post_status'       => 'publish',
                        'order'             => 'DESC',
                        'posts_per_page'    => -1,
                        'meta_query' => array(
                            array(
                                'key' => 'week_fixture',
                                'value' => $weekFixture,
                                'compare' => '=',
                            ),
                            array(
                                'key' => 'user_id',
                                'value' => $user->ID,
                                'compare' => '=',
                            ),
                        )
                    );
                    $customWeekWinnings = get_posts( $args );
                    if (isset($customWeekWinnings) && is_array($customWeekWinnings) && count($customWeekWinnings) > 0) {
                        $score = 0;
                        foreach ( $customWeekWinnings as $customWeekWinning ) {
                            $winningScore = get_post_meta( $customWeekWinning->ID, 'score', true );
                            $score += (int) $winningScore;
                        } ?>
                        <tr>
                            <td><?php echo ++$count; ?></td>
                            <td><?php echo $user->user_login; ?></td>
                            <td><?php echo $score; ?></td>
                            <td>0</td>
                        </tr>
                    <?php }
                }
                if ($count == 0) { ?>
                    <tr>
                        <td colspan="4">There is no record under this week.</td>
                    </tr>
                <?php }
            ?>
        </tbody>
    </table>
</div>