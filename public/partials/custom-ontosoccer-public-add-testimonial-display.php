<div class="custom-ontosoccer-add-testimonial-shortcode">
    <div class="add-testimonial-form">
        <h2 class="moverspackers-widget-heading moverspackers-color mb-3">Submit Testimonial</h2>
        <p class="hint-text mb-3">Please tell us about your thoughts.</p>
        <?php if (isset($_GET['success']) && !empty($_GET['success']) && $_GET['success'] == 'true') : ?>
        <div class="alert alert-success alert-dismissible show" role="alert">
            <strong>Your testimonial is successfully submitted!</strong> We will try to pubish it soon.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif; ?>
        <?php if (isset($_GET['success']) && !empty($_GET['success']) && $_GET['success'] == 'false') : ?>
        <div class="alert alert-danger alert-dismissible show" role="alert">
            <strong>Oh Snap!</strong> Something wrong went to server.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif; ?>
        <form action="<?php echo admin_url('admin-post.php'); ?>" class="add-testimonial-form" method="POST">
            <input type="hidden" name="action" value="custom_ontosoccer_add_testimonial">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="display_name">Display Name</label>
                        <input type="text" class="form-control" id="display_name" name="display_name">
                        <small class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="email_address">Email Address</label>
                        <input type="text" class="form-control email-address" id="email_address" name="email_address">
                        <small class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="mobile_number">Mobile Number</label>
                        <input type="tel" class="form-control mobile-number" id="mobile_number" name="mobile_number">
                        <small class="form-text text-muted mobile-number pl-0">We'll never share your email with anyone else.</small>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="rating">Rating</label>
                        <select name="rating" id="rating" class="form-control">
                            <?php for ($i = 10; $i > 0; $i--) { ?>
                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                            <?php } ?>
                        </select>
                        <small class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="message">Message</label>
                        <textarea name="message" id="message" class="form-control" cols="30" rows="10" maxlength="250"></textarea>
                        <small class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <button type="button" class="btn btn-primary btn-block btn-submit">Submit Now</button>
                    </div>
                </div>
            </div>       
        </form>
    </div>
</div>