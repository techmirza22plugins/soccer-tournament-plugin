<div class="custom-ontosoccer-forgot-password-shortcode">
    <div class="forgot-password-form">
        <h2 class="moverspackers-widget-heading moverspackers-color mb-3">Forgot Password</h2>
        <?php if (isset($_GET['password-success']) && !empty($_GET['password-success']) && $_GET['password-success'] == 'true') : ?>
        <div class="alert alert-success alert-dismissible show" role="alert">
            <strong>Success! Password Recovery email has been sent to your inbox!</strong>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif; ?>
        <form action="<?php echo admin_url('admin-post.php'); ?>" class="forgot-password-form" method="POST">
            <input type="hidden" name="action" value="custom_ontosoccer_forgot_password">
            <div class="row">
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control email-address" id="email" name="email">
                        <small class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <button type="button" class="btn btn-primary btn-block btn-submit">Submit</button>
                    </div>
                </div>
            </div>       
        </form>
        <div class="text-center">Already have an account? <a href="#">Login Here</a></div>
    </div>
</div>