<?php
    $customOntoSoccerSettings = get_option('custom_ontosoccer_settings', array());
    $custom_ontosoccer_registration_page_id = $customOntoSoccerSettings['custom_ontosoccer_registration_page_id'];
    $custom_ontosoccer_login_page_id = $customOntoSoccerSettings['custom_ontosoccer_login_page_id'];
    $weekFixture = '' . date('Y/m/d');
    $weekFixture = new DateTime($weekFixture);
    $weekFixture = $weekFixture->format("W");
    $args = array(
        'post_type'         => 'custom-match',
        'orderby'           => 'ID',
        'post_status'       => 'publish',
        'order'             => 'DESC',
        'posts_per_page'    => -1,
        'meta_query' => array(
            array(
                'key' => 'match_week',
                'value' => $weekFixture,
                'compare' => '=',
            ),
        )
    );
    $customMatches = get_posts( $args );
    $weekFixture = '' . date('Y/m/d');
    $weekFixture = new DateTime($weekFixture);
    $weekFixture = $weekFixture->format("W");
    $user_id = get_current_user_id();
    if (isset($user_id) && !empty($user_id)) {
        $args = array(
            'post_type'         => 'custom-prediction',
            'orderby'           => 'ID',
            'post_status'       => 'publish',
            'order'             => 'DESC',
            'posts_per_page'    => 1,
            'meta_query' => array(
                'relation' => 'AND',
                array(
                    'key' => 'week_fixture',
                    'value' => $weekFixture,
                    'compare' => '=',
                ),
                array(
                    'key' => 'user_id',
                    'value' => get_current_user_id(),
                    'compare' => '=',
                )
            )
         );
        $predictedMatches = get_posts( $args );
    }
?>
<style>
    .table-responsive td {
        border: none;
        text-align: left;
    }
</style>
<div class="custom-ontosoccer-week-fixtures-shortcode">
    <h2 class="week-fixutre moverspackers-widget-heading moverspackers-color mb-3" data-week-fixture="<?php echo $weekFixture; ?>">Week <?php echo $weekFixture; ?> Fixtures</h2>
    <div class="card">
        <div class="card-body">
            <ul class="list-group list-group-flush">
                <li class="list-group-item">
                    <div class="alert alert-danger" role="alert"></div>
                    <?php if (isset($predictedMatches) && is_array($predictedMatches) && count($predictedMatches) > 0) : ?>
                    <div class="alert alert-danger" role="alert" style="display: block;">
                        Sorry, you've already submitted your prediction of matches for this week.
                    </div>
                    <?php endif; ?>
                    <div class="alert alert-success" role="alert"></div>
                    <h6 style="display: inline;">NUMBER OF MATCHES YOU ARE PREDICTING FOR:</h6>
                    <select name="predicting_matches" id="predicting-matches" class="form-control" style="display: inline; width: auto;">
                        <?php for ($i = 3; $i <= 6; $i++) { ?>
                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                        <?php } ?>
                    </select>
                    <div class="table table-bordered table-responsive mt-4">
                        <table class="table">
                            <tbody>
                                <?php if (isset($customMatches) && is_array($customMatches) && count($customMatches) > 0) { ?>
                                    <?php foreach ($customMatches as $match) {
                                        $match_date = get_post_meta( $match->ID, 'match_date', true );
                                        $first_team_goals = get_post_meta( $match->ID, 'first_team_goals', true );
                                        $second_team_goals = get_post_meta( $match->ID, 'second_team_goals', true ); ?>
                                        <?php if (isset($first_team_goals) && !empty($first_team_goals) && isset($second_team_goals) && !empty($second_team_goals)) { ?>
                                            <tr>
                                                <td><?php echo date('d/m/Y', strtotime(get_post_meta($match->ID, 'match_date', true))); ?></td>
                                                <td><?php echo get_post_meta($match->ID, 'first_team', true); ?></td>
                                                <td colspan="2"><?php echo get_post_meta($match->ID, 'second_team', true); ?></td>
                                                <td><span class="badge badge-primary"><?php echo get_post_meta($match->ID, 'first_team_goals', true); ?> - <?php echo get_post_meta($match->ID, 'second_team_goals', true); ?></span></td>
                                            </tr>
                                        <?php } else {
                                            $start_date = new DateTime( get_post_meta($match->ID, 'match_date', true) . ' ' . get_post_meta($match->ID, 'match_time', true) );
                                            $since_start = $start_date->diff( new DateTime( '' . date('Y-m-d H:i') ) );
                                            $minutes = $since_start->days * 24 * 60;
                                            $minutes += $since_start->h * 60;
                                            $minutes += $since_start->i;
                                            ?>
                                            <?php if ('' . date('d/m/Y', strtotime(get_post_meta($match->ID, 'match_date', true))) < '' . date('d/m/Y') || $minutes < 30) { ?>
                                                <tr>
                                                    <td><?php echo date('d/m/Y', strtotime(get_post_meta($match->ID, 'match_date', true))); ?></td>
                                                    <td><?php echo get_post_meta($match->ID, 'first_team', true); ?></td>
                                                    <td colspan="2"><?php echo get_post_meta($match->ID, 'second_team', true); ?></td>
                                                    <td><span class="badge badge-danger">Closed</span></td>
                                                </tr>
                                            <?php } else { ?>
                                                <tr class="match-entry" data-matchID="<?php echo $match->ID; ?>">
                                                    <td style="padding-top: 10px;"><?php echo date('d/m/Y', strtotime(get_post_meta($match->ID, 'match_date', true))); ?></td>
                                                    <td style="padding-top: 10px;"><?php echo get_post_meta($match->ID, 'first_team', true); ?></td>
                                                    <td>
                                                        <select name="first_team_score" id="first-team-score-<?php echo $match->ID; ?>" class="form-control" style="width: auto;">
                                                            <option value=""></option>
                                                            <?php for ($i = 0; $i <= 10; $i++) { ?>
                                                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </td>
                                                    <td style="padding-top: 10px;"><?php echo get_post_meta($match->ID, 'second_team', true); ?></td>
                                                    <td>
                                                        <select name="second_team_score" id="second-team-score-<?php echo $match->ID; ?>" class="form-control" style="width: auto;">
                                                            <option value=""></option>
                                                            <?php for ($i = 0; $i <= 10; $i++) { ?>
                                                                <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    </td>
                                                </tr>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } else { ?>
                                <tr>
                                    <td colspan="5">There is no record related to Week Fixtures.</td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </li>
                <hr style="margin-bottom: 0rem; margin-top: 0rem;">
                <?php $userID = get_current_user_id(); ?>
                <?php if (isset($customMatches) && is_array($customMatches) && count($customMatches) > 0) : ?>
                    <?php if ( isset($userID) && !empty($userID) ) { ?>
                        <li class="list-group-item" style="padding-bottom: 0rem;">
                            <button type="button" class="btn btn-primary submit-prediction-btn">Submit Predictions</button>
                        </li>
                    <?php } else { ?>
                        <li class="list-group-item" style="padding-bottom: 0rem;">
                            <span>You need to be logged in to make predictions. Please <a href="<?php echo get_permalink($custom_ontosoccer_login_page_id); ?>">Login</a> or <a href="<?php echo get_permalink($custom_ontosoccer_registration_page_id); ?>">Register</a></span>
                        </li>
                    <?php } ?>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</div>