<div class="custom-ontosoccer-register-shortcode">
    <div class="signup-form">
        <h2 class="moverspackers-widget-heading moverspackers-color mb-3">Register</h2>
        <?php if (isset($_GET['register']) && !empty($_GET['register']) && $_GET['register'] == 'true') : ?>
        <div class="alert alert-success alert-dismissible show" role="alert">
            <strong>Registration successfully done!</strong> Please check your email for validation.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif; ?>
        <?php if (isset($_GET['register']) && !empty($_GET['register']) && $_GET['register'] == 'false') : ?>
        <div class="alert alert-danger alert-dismissible show" role="alert">
            <strong>Oh Snap!</strong> Something wrong went to server.
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <?php endif; ?>
        <form action="<?php echo admin_url('admin-post.php'); ?>" class="registration-form" method="POST">
            <input type="hidden" name="action" value="custom_ontosoccer_register">
            <div class="row">
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="first_name">First Name</label>
                        <input type="text" class="form-control" id="first_name" name="first_name">
                        <small class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="form-group">
                        <label for="last_name">Last Name</label>
                        <input type="text" class="form-control" id="last_name" name="last_name">
                        <small class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="email_address">Email Address</label>
                        <input type="text" class="form-control email-address" id="email_address" name="email_address">
                        <small class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="date_of_birth">Date of Birth</label>
                        <input type="text" class="form-control datepicker" id="date_of_birth" name="date_of_birth">
                        <small class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="mobile_number">Mobile Number</label>
                        <input type="tel" class="form-control mobile-number" id="mobile_number" name="mobile_number">
                        <small class="form-text text-muted mobile-number pl-0">We'll never share your email with anyone else.</small>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="twitter_username">Twitter Username</label>
                        <input type="text" class="form-control" id="twitter_username" name="twitter_username">
                        <small class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="telecom_provider">Telecom Provider</label>
                        <select name="telecom_provider" id="telecom_provider" class="form-control">
                            <option value="MTN">MTN</option>
                            <option value="Airtel">Airtel</option>
                            <option value="Glo">Glo</option>
                            <option value="9Mobile">9Mobile</option>
                            <option value="Other">Other</option>
                        </select>
                        <small class="form-text text-muted">We'll never share your email with anyone else.</small>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <button type="button" class="btn btn-primary btn-block btn-submit">Register Now</button>
                    </div>
                </div>
            </div>       
        </form>
        <div class="text-center">Already have an account? <a href="#">Sign in</a></div>
    </div>
</div>