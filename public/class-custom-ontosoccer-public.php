<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://wppb.me/
 * @since      1.0.0
 *
 * @package    Custom_Ontosoccer
 * @subpackage Custom_Ontosoccer/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Custom_Ontosoccer
 * @subpackage Custom_Ontosoccer/public
 * @author     Tech Mirza <talhamirza2@gmail.com>
 */
class Custom_Ontosoccer_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Custom_Ontosoccer_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Custom_Ontosoccer_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name.'-bootstrap-css', plugin_dir_url( __FILE__ ) . 'css/bootstrap.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name.'-intlTelInput-css', plugin_dir_url( __FILE__ ) . 'css/intlTelInput.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name.'-bootstrap-datepicker-css', plugin_dir_url( __FILE__ ) . 'css/bootstrap-datepicker3.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name.'-simple-line-icons-css', 'https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.5.5/css/simple-line-icons.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/custom-ontosoccer-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Custom_Ontosoccer_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Custom_Ontosoccer_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name.'-jquery-js', plugin_dir_url( __FILE__ ) . 'js/jquery-3.5.1.slim.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name.'-popper-js', plugin_dir_url( __FILE__ ) . 'js/bootstrap.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name.'-bootstrap-js', plugin_dir_url( __FILE__ ) . 'js/bootstrap.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name.'-intlTelInput-js', plugin_dir_url( __FILE__ ) . 'js/intlTelInput.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name.'-bootstrap-datepicker-js', plugin_dir_url( __FILE__ ) . 'js/bootstrap-datepicker.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/custom-ontosoccer-public.js', array( 'jquery' ), $this->version, false );
		wp_localize_script( $this->plugin_name, 'my_ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );

	}

	public function add_shortcodes() {
		add_shortcode( 'custom-ontosoccer-registration', array($this, 'custom_ontosoccer_registration_shortcode') );
		add_shortcode( 'custom-ontosoccer-login', array($this, 'custom_ontosoccer_login_shortcode') );
		add_shortcode( 'custom-ontosoccer-validate-email', array($this, 'custom_ontosoccer_validate_email_shortcode') );
		add_shortcode( 'custom-ontosoccer-change-password', array($this, 'custom_ontosoccer_change_password_shortcode') );
		add_shortcode( 'custom-ontosoccer-edit-profile', array($this, 'custom_ontosoccer_edit_profile_shortcode') );
		add_shortcode( 'custom-ontosoccer-forgot-password', array($this, 'custom_ontosoccer_forgot_password_shortcode') );
		add_shortcode( 'custom-ontosoccer-contact-us', array($this, 'custom_ontosoccer_contact_us_shortcode') );
		add_shortcode( 'custom-ontosoccer-add-testimonial', array($this, 'custom_ontosoccer_add_testimonial_shortcode') );
		add_shortcode( 'custom-ontosoccer-list-testimonials', array($this, 'custom_ontosoccer_list_testimonials_shortcode') );
		add_shortcode( 'custom-ontosoccer-week-fixtures', array($this, 'custom_ontosoccer_week_fixtures_shortcode') );
		add_shortcode( 'custom-ontosoccer-week-fixtures-standings', array($this, 'custom_ontosoccer_week_fixtures_standings_shortcode') );
		add_shortcode( 'custom-ontosoccer-month-fixtures-standings', array($this, 'custom_ontosoccer_month_fixtures_standings_shortcode') );
	}

	public function custom_ontosoccer_registration_shortcode() {
		ob_start();
        include_once 'partials/custom-ontosoccer-public-registration-display.php';
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
	}

	public function custom_ontosoccer_login_shortcode() {
		ob_start();
        include_once 'partials/custom-ontosoccer-public-login-display.php';
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
	}

	public function custom_ontosoccer_validate_email_shortcode() {
		ob_start();
        include_once 'partials/custom-ontosoccer-public-validate-email-display.php';
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
	}

	public function custom_ontosoccer_change_password_shortcode() {
		ob_start();
        include_once 'partials/custom-ontosoccer-public-change-password-display.php';
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
	}

	public function custom_ontosoccer_edit_profile_shortcode() {
		ob_start();
        include_once 'partials/custom-ontosoccer-public-profile-edit-display.php';
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
	}

	public function custom_ontosoccer_forgot_password_shortcode() {
		ob_start();
        include_once 'partials/custom-ontosoccer-public-forgot-password-display.php';
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
	}

	public function custom_ontosoccer_contact_us_shortcode() {
		ob_start();
        include_once 'partials/custom-ontosoccer-public-contact-us-display.php';
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
	}

	public function custom_ontosoccer_add_testimonial_shortcode() {
		ob_start();
        include_once 'partials/custom-ontosoccer-public-add-testimonial-display.php';
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
	}

	public function custom_ontosoccer_list_testimonials_shortcode() {
		ob_start();
        include_once 'partials/custom-ontosoccer-public-list-testimonials-display.php';
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
	}

	public function custom_ontosoccer_week_fixtures_shortcode() {
		ob_start();
        include_once 'partials/custom-ontosoccer-public-week-fixtures-display.php';
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
	}

	public function custom_ontosoccer_week_fixtures_standings_shortcode() {
		ob_start();
        include_once 'partials/custom-ontosoccer-public-week-fixtures-standings-display.php';
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
	}

	public function custom_ontosoccer_month_fixtures_standings_shortcode() {
		ob_start();
        include_once 'partials/custom-ontosoccer-public-month-fixtures-standings-display.php';
        $output = ob_get_contents();
        ob_end_clean();
        return $output;
	}

}
