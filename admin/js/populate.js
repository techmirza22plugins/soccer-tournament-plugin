jQuery (function($) {
    
    $(document).ready(function() {
        if (window.location.href.includes("edit.php?post_type=custom-match")) {
            var wp_inline_edit_function = inlineEditPost.edit;
            inlineEditPost.edit = function( post_id ) {
                wp_inline_edit_function.apply( this, arguments );
                var id = 0;
                if ( typeof( post_id ) == "object" ) { // if it is object, get the ID number
                    id = parseInt( this.getId( post_id ) );
                }
                if ( id > 0 ) {
                    var specific_post_edit_row = $( "#edit-" + id ),
                        specific_post_row = $( "#post-" + id ),
                        first_team_score = $( ".column-first_team_score", specific_post_row ).text(),
                        second_team_score = $( ".column-second_team_score", specific_post_row ).text();
                    if (first_team_score != "Not set.") {
                        $( ":input[name='first_team_score']", specific_post_edit_row ).val( first_team_score );
                    }
                    if (second_team_score != "Not set.") {
                        $( ":input[name='second_team_score']", specific_post_edit_row ).val( second_team_score );
                    }
                }
            }
        }
    });

});