<?php

require_once 'libs/Twilio/autoload.php';
use Twilio\Rest\Client;
require_once 'libs/Twitter/autoload.php';
use Abraham\TwitterOAuth\TwitterOAuth;

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://wppb.me/
 * @since      1.0.0
 *
 * @package    Custom_Ontosoccer
 * @subpackage Custom_Ontosoccer/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Custom_Ontosoccer
 * @subpackage Custom_Ontosoccer/admin
 * @author     Tech Mirza <talhamirza2@gmail.com>
 */
class Custom_Ontosoccer_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Custom_Ontosoccer_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Custom_Ontosoccer_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name.'-select2-css', plugin_dir_url( __FILE__ ) . 'css/select2.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name.'-jquery-dataTables-css', plugin_dir_url( __FILE__ ) . 'css/jquery.dataTables.min.css', array(), $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/custom-ontosoccer-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts( $pagehook ) {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Custom_Ontosoccer_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Custom_Ontosoccer_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		if ( ! did_action( 'wp_enqueue_media' ) ) {
			wp_enqueue_media();
		}
		wp_enqueue_script( $this->plugin_name.'-select2-js', plugin_dir_url( __FILE__ ) . 'js/select2.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name.'-jquery-dataTables-js', plugin_dir_url( __FILE__ ) . 'js/jquery.dataTables.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name.'-progressbar-js', plugin_dir_url( __FILE__ ) . 'js/progressbar.min.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name.'-populate-js', plugin_dir_url( __FILE__ ) . 'js/populate.js', array( 'jquery' ), $this->version, false );
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/custom-ontosoccer-admin.js', array( 'jquery' ), $this->version, false );

	}

	public function custom_ontosoccer_plugin_settings() {
		register_setting( 'custom-ontosoccer-settings', 'custom_ontosoccer_settings' );
		register_setting( 'custom-ontosoccer-contact-us', 'custom_ontosoccer_contact_us' );
		register_setting( 'custom-ontosoccer-testimonial', 'custom_ontosoccer_testimonial' );
		register_setting( 'custom-ontosoccer-subscription', 'custom_ontosoccer_subscription' );
		register_setting( 'custom-ontosoccer-prediction', 'custom_ontosoccer_prediction' );
		register_setting( 'custom-ontosoccer-email-template', 'custom_ontosoccer_email_template' );
		register_setting( 'custom-ontosoccer-send-newsletter-email', 'custom_ontosoccer_send_newsletter_email' );
		register_setting( 'custom-ontosoccer-send-user-email', 'custom_ontosoccer_send_user_email' );
		register_setting( 'custom-ontosoccer-send-sms', 'custom_ontosoccer_send_sms' );
		register_setting( 'custom-ontosoccer-twilio-settings', 'custom_ontosoccer_twilio_settings' );
		register_setting( 'custom-ontosoccer-twitter-api-settings', 'custom_ontosoccer_twitter_api_settings' );
	}

	public function custom_ontosoccer_plugin_menu() {
		add_menu_page(
			'Custom OntoSoccer Settings',
			'Custom OntoSoccer Settings',
			'manage_options',
			'custom-ontosoccer-settings',
			array($this, 'plugin_settings_callback')
		);
		add_submenu_page(
			'custom-ontosoccer-settings',
			'Contact Us Entries',
			'Contact Us Entries',
			'manage_options',
			'custom-ontosoccer-contact-us',
			array($this, 'plugin_contact_us_callback')
		);
		add_submenu_page(
			'custom-ontosoccer-settings',
			'Testimonial Entries',
			'Testimonial Entries',
			'manage_options',
			'custom-ontosoccer-testimonial',
			array($this, 'plugin_testimonial_callback')
		);
		add_submenu_page(
			'custom-ontosoccer-settings',
			'Subscription Entries',
			'Subscription Entries',
			'manage_options',
			'custom-ontosoccer-subscription',
			array($this, 'plugin_subscription_callback')
		);
		add_submenu_page(
			'custom-ontosoccer-settings',
			'Prediction Entries',
			'Prediction Entries',
			'manage_options',
			'custom-ontosoccer-prediction',
			array($this, 'plugin_prediction_callback')
		);
		add_submenu_page(
			'custom-ontosoccer-settings',
			'Send User Email',
			'Send User Email',
			'manage_options',
			'custom-ontosoccer-send-user-email',
			array($this, 'plugin_send_user_email_callback')
		);
		add_submenu_page(
			'custom-ontosoccer-settings',
			'Send Newsletter Email',
			'Send Newsletter Email',
			'manage_options',
			'custom-ontosoccer-send-newsletter-email',
			array($this, 'plugin_send_newsletter_email_callback')
		);
		add_submenu_page(
			'custom-ontosoccer-settings',
			'Send SMS',
			'Send SMS',
			'manage_options',
			'custom-ontosoccer-send-sms',
			array($this, 'plugin_send_sms_callback')
		);
		add_submenu_page(
			'edit.php?post_type=custom-match',
			'Import CSV',
			'Import CSV',
			'manage_options',
			'custom-ontosoccer-import-matches',
			array($this, 'plugin_import_matches_callback')
		);
		add_submenu_page(
			'custom-ontosoccer-settings',
			'Import Pin Numbers',
			'Import Pin Numbers',
			'manage_options',
			'custom-ontosoccer-import-pin-numbers',
			array($this, 'plugin_import_pin_numbers_callback')
		);
		add_submenu_page(
			'custom-ontosoccer-settings',
			'Send Pin Number to User',
			'Send Pin Number to User',
			'manage_options',
			'custom-ontosoccer-send-pin-number-to-user',
			array($this, 'plugin_send_pin_number_to_user_callback')
		);
	}

	public function plugin_settings_callback() {
		require('partials/custom-ontosoccer-admin-display.php');
	}

	public function plugin_contact_us_callback() {
		require('partials/custom-ontosoccer-admin-contact-us-display.php');
	}

	public function plugin_testimonial_callback() {
		require('partials/custom-ontosoccer-admin-testimonial-display.php');
	}

	public function plugin_subscription_callback() {
		require('partials/custom-ontosoccer-admin-subscription-display.php');
	}

	public function plugin_prediction_callback() {
		require('partials/custom-ontosoccer-admin-prediction-display.php');
	}

	public function plugin_send_user_email_callback() {
		require('partials/custom-ontosoccer-admin-send-user-email-display.php');
	}

	public function plugin_send_newsletter_email_callback() {
		require('partials/custom-ontosoccer-admin-send-newsletter-email-display.php');
	}

	public function plugin_send_sms_callback() {
		require('partials/custom-ontosoccer-admin-send-sms-display.php');
	}

	public function plugin_import_matches_callback() {
		require('partials/custom-ontosoccer-admin-import-matches-display.php');
	}

	public function plugin_import_pin_numbers_callback() {
		require('partials/custom-ontosoccer-admin-import-pin-numbers-display.php');
	}

	public function plugin_send_pin_number_to_user_callback() {
		require('partials/custom-ontosoccer-admin-send-pin-number-to-user-display.php');
	}

	public function custom_ontosoccer_admin_init() {
		// Set UI labels for Custom Post Type
		$labels = array(
			'name'                => _x( 'Matches', 'Post Type General Name', 'roster-automation' ),
			'singular_name'       => _x( 'Match', 'Post Type Singular Name', 'roster-automation' ),
			'menu_name'           => __( 'Matches', 'roster-automation' ),
			'parent_item_colon'   => __( 'Parent Match', 'roster-automation' ),
			'all_items'           => __( 'All Matches', 'roster-automation' ),
			'view_item'           => __( 'View Match', 'roster-automation' ),
			'add_new_item'        => __( 'Add New Match', 'roster-automation' ),
			'add_new'             => __( 'Add New', 'roster-automation' ),
			'edit_item'           => __( 'Edit Match', 'roster-automation' ),
			'update_item'         => __( 'Update Match', 'roster-automation' ),
			'search_items'        => __( 'Search Match', 'roster-automation' ),
			'not_found'           => __( 'Not Found', 'roster-automation' ),
			'not_found_in_trash'  => __( 'Not found in Trash', 'roster-automation' ),
		);
		 
		// Set other options for Custom Post Type
		 
		$args = array(
			'label'               => __( 'Matches', 'roster-automation' ),
			'description'         => __( 'Matches', 'roster-automation' ),
			'labels'              => $labels,
			// Features this CPT supports in Post Editor
			'supports'            => array( 'title' ),
			/* A hierarchical CPT is like Pages and can have
			* Parent and child items. A non-hierarchical CPT
			* is like Posts.
			*/ 
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 5,
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
			'capability_type'     => 'post',
		);
		 
		// Registering your Custom Post Type
		register_post_type( 'custom-match', $args );
	}

	public function custom_match_meta_box() {
		add_meta_box(
            'custom_match_details_metabox',
            __('Match Details','roster-automation'),
            array( $this, 'custom_match_meta_box_callback' )
		);
		if (isset($_GET['post']) && !empty($_GET['post']) && isset($_GET['action']) && !empty($_GET['action']) && $_GET['action'] == 'edit') {
			add_meta_box(
				'custom_match_goals_details_metabox',
				__('Match Goals Details','roster-automation'),
				array( $this, 'custom_match_goals_meta_box_callback' )
			);
		}
	}

	public function custom_match_meta_box_callback( $post ) {
		include('partials/custom-match-metabox.php');
	}

	public function custom_match_goals_meta_box_callback( $post ) {
		include('partials/custom-match-goals-metabox.php');
	}

	public function custom_match_save_details( $post_id, $post, $update ) {
		if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
            return;
		}
		if( defined('DOING_AJAX') && DOING_AJAX ) {
			return;
		}
		// Update Meta Data
		update_post_meta( $post_id, 'first_team', $_POST['first_team']);
		update_post_meta( $post_id, 'second_team', $_POST['second_team']);
		$date = date_create( $_POST['match_date'] );
		$date = '' . date_format( $date, 'Y/m/d' );
		$week = new DateTime($date);
    	$week = $week->format("W");
		if ( empty( $_POST['first_team_goals'] ) ) {
			update_post_meta( $post_id, 'match_week', $week);
			update_post_meta( $post_id, 'match_date', $_POST['match_date']);
			update_post_meta( $post_id, 'match_time', $_POST['match_time']);
		} else {
			update_post_meta( $post_id, 'first_team_goals', $_POST['first_team_goals']);
			update_post_meta( $post_id, 'second_team_goals', $_POST['second_team_goals']);
			$match_week = get_post_meta( $post_id, 'match_week', true );
			$first_team_goals = $_POST['first_team_goals'];
			$second_team_goals = $_POST['second_team_goals'];
			$args = array(
				'post_type'=> 'custom-prediction',
				'orderby'    => 'ID',
				'post_status' => 'publish',
				'order'    => 'DESC',
				'posts_per_page' => -1,
				'meta_query' => array(
					'relation' => 'AND',
					array(
						'key' => 'week_fixture',
						'value' => $match_week,
						'compare' => '=',
					)
				)
			);
			$customPredictions = get_posts( $args );
			if (isset($customPredictions) && is_array($customPredictions) && count($customPredictions) > 0) {
				foreach ($customPredictions as $prediction) {
					$predictionMatches = get_post_meta( $prediction->ID, 'matches', true );
					$user_id = get_post_meta( $prediction->ID, 'user_id', true );
					foreach ($predictionMatches as $predictionMatch) {
						if ($predictionMatch['ID'] == $post_id) {
							$args = array(
								'post_type'=> 'custom-week-win',
								'orderby'    => 'ID',
								'post_status' => 'publish',
								'order'    => 'DESC',
								'posts_per_page' => -1,
								'meta_query' => array(
									'relation' => 'AND',
									array(
										'key' => 'week_fixture',
										'value' => $match_week,
										'compare' => '=',
									),
									array(
										'key' => 'match_id',
										'value' => $post_id,
										'compare' => '=',
									),
									array(
										'key' => 'user_id',
										'value' => $user_id,
										'compare' => '=',
									),
								)
							);
							$foundWinning = get_posts( $args );
							if (isset($foundWinning) && is_array($foundWinning) && count($foundWinning)) {
								$score = 0;
								if ($first_team_goals == $predictionMatch['firstTeamScore'] && $second_team_goals == $predictionMatch['secondTeamScore']) {
									$score = 3;
								} else if (((int) $first_team_goals - (int) $second_team_goals) == ((int) $predictionMatch['firstTeamScore'] - (int) $predictionMatch['secondTeamScore'])) {
									$score = 1;
								}
								update_post_meta( $foundWinning[0]->ID, 'score', $score );
							} else {
								$new_post = array(
									'post_title' => rand(1000000000, 9999999999),
									'post_status' => 'publish',
									'post_date' => date('Y-m-d H:i:s'),
									'post_author' => get_current_user_id(),
									'post_type' => 'custom-week-win',
								);
								$custom_week_win_post_id = wp_insert_post($new_post);
								update_post_meta( $custom_week_win_post_id, 'match_id', $post_id );
								update_post_meta( $custom_week_win_post_id, 'user_id', $user_id );
								update_post_meta( $custom_week_win_post_id, 'week_fixture', $match_week );
								$score = 0;
								if ($first_team_goals == $predictionMatch['firstTeamScore'] && $second_team_goals == $predictionMatch['secondTeamScore']) {
									$score = 3;
								} else if (((int) $first_team_goals - (int) $second_team_goals) == ((int) $predictionMatch['firstTeamScore'] - (int) $predictionMatch['secondTeamScore'])) {
									$score = 1;
								}
								update_post_meta( $custom_week_win_post_id, 'score', $score );
							}
						}
					}
				}
			}
		}
	}

	public function check_email_address() {
		$email = $_POST['email'];
		echo email_exists($email);
	}

	public function check_twitter_username() {
		$username = $_POST['username'];
		$customOntosoccerTwitterAPISettings = get_option('custom_ontosoccer_twitter_api_settings', array());
		$bearer_token = $customOntosoccerTwitterAPISettings['bearer_token'];
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api.twitter.com/2/users/by/username/".$username,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
				CURLOPT_HTTPHEADER => array(
					"Authorization: Bearer " . $bearer_token,
				),
		));
		$response = curl_exec($curl);
		curl_close($curl);
		$result = json_decode($response);
		if ( $result->data ) {
			echo true;
		} else {
			echo false;
		}
	}

	public function generateRandomString($length = 10) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	public function custom_ontosoccer_register() {
		$first_name = $_POST['first_name'];
		$last_name = $_POST['last_name'];
		$email_address = $_POST['email_address'];
		$username = strstr($email_address, '@', true);
		$date_of_birth = $_POST['date_of_birth'];
		$mobile_number = $_POST['mobile_number'];
		$twitter_username = $_POST['twitter_username'];
		$telecom_provider = $_POST['telecom_provider'];
		$result = wp_create_user( $username, $username, $email_address );
		if (is_wp_error($result)) {
			$error = $result->get_error_message();
			wp_redirect( $_SERVER['HTTP_REFERER'] . '?register=false' );
		} else {
			$user = get_user_by('id', $result);
			update_user_meta( $user->ID, 'first_name', $first_name );
			update_user_meta( $user->ID, 'last_name', $last_name );
			update_user_meta( $user->ID, 'date_of_birth', $date_of_birth );
			update_user_meta( $user->ID, 'mobile_number', $mobile_number );
			update_user_meta( $user->ID, 'twitter_username', $twitter_username );
			update_user_meta( $user->ID, 'telecom_provider', $telecom_provider );
			$validation_code = $this->generateRandomString();
			update_user_meta( $user->ID, 'validation_code', $validation_code );
			$customOntoSoccerSettings = get_option('custom_ontosoccer_settings', array());
            $custom_ontosoccer_validate_email_page_id = $customOntoSoccerSettings['custom_ontosoccer_validate_email_page_id'];
			$customOntosoccerEmailTemplate = get_option('custom_ontosoccer_email_template', array());
			$email_template = $customOntosoccerEmailTemplate['email_template'];
			$to = $email_address;
			$subject = 'Validate your Account';
			$from = get_option('admin_email');
			// To send HTML mail, the Content-type header must be set
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			// Create email headers
			$headers .= 'From: '.$from."\r\n".
				'Reply-To: '.$from."\r\n" .
				'X-Mailer: PHP/' . phpversion();
			// Compose a simple HTML email message
			$message = '';
			$message .= '<h3>Hi '.$first_name.' '.$last_name.'!</h3>';
			$message .= '<p style="font-weight: bold;">Your validation code is: '.$validation_code.'</p>';
			$message .= '<p>Please go to the link: <a href="'.get_permalink($custom_ontosoccer_validate_email_page_id).'?user='.$user->ID.'">Validate your Email</a> to validate your email and then you will be able to login to your account.</p>';
			$message .= '<p>Thanks for your attention!</p>';
			$message .= '';
			$email_template = str_replace("{{CODE}}", html_entity_decode(htmlentities(wpautop($message))), $email_template);
			// Sending Mail
			wp_mail($to, $subject, $email_template, $headers);
			$customOntoSoccerSettings = get_option('custom_ontosoccer_settings', array());
			$custom_ontosoccer_registration_page_id = $customOntoSoccerSettings['custom_ontosoccer_registration_page_id'];
			wp_redirect( get_permalink($custom_ontosoccer_registration_page_id) . '?register=true' );
		}
	}

	public function wpse27856_set_content_type() {
		return "text/html";
	}

	public function check_validation_code() {
		$validation_code = $_POST['validation_code'];
		$user_id = $_POST['user_id'];
		$user_validation_code = get_user_meta( (int) $user_id, 'validation_code', true );
		if ($user_validation_code == $validation_code) {
			echo true;
		} else {
			echo false;
		}
	}

	public function custom_ontosoccer_validate_email() {
		$customOntoSoccerSettings = get_option('custom_ontosoccer_settings', array());
		$custom_ontosoccer_change_password_page_id = $customOntoSoccerSettings['custom_ontosoccer_change_password_page_id'];
		if (isset($_POST['verify_password']) && !empty($_POST['verify_password']) && $_POST['verify_password'] == 'true') {
			wp_redirect( get_permalink($custom_ontosoccer_change_password_page_id) . '?user='.$_POST['user_id'].'&verify-password=true' );
		} else {
			wp_redirect( get_permalink($custom_ontosoccer_change_password_page_id) . '?user='.$_POST['user_id'].'&change-password=true' );
		}
	}

	public function custom_ontosoccer_change_password() {
		$user_id = $_POST['user_id'];
		$password = $_POST['password'];
		$repeat_password = $_POST['repeat_password'];
		wp_set_password( $password, (int) $user_id );
		$customOntoSoccerSettings = get_option('custom_ontosoccer_settings', array());
		$custom_ontosoccer_login_page_id = $customOntoSoccerSettings['custom_ontosoccer_login_page_id'];
		if (isset($_POST['verify_password']) && !empty($_POST['verify_password']) && $_POST['verify_password'] == 'true') {
			wp_redirect( get_permalink($custom_ontosoccer_login_page_id) . '?password-changed=true' );
		} else {
			wp_redirect( get_permalink($custom_ontosoccer_login_page_id) . '?account-validated=true' );
		}
	}

	public function custom_ontosoccer_login() {
		$email_username = $_POST['email_username'];
		$password = $_POST['password'];
		// Authorize
		$auth = wp_authenticate( $email_username, $password );
		// Error checking
		if ( is_wp_error( $auth ) ) {
			$customOntoSoccerSettings = get_option('custom_ontosoccer_settings', array());
			$custom_ontosoccer_login_page_id = $customOntoSoccerSettings['custom_ontosoccer_login_page_id'];
			wp_redirect( get_permalink($custom_ontosoccer_login_page_id) . '?success=false' );
		}
		else {
			$user_id = $auth->ID;
			$ban = get_user_meta( $user_id, 'ban', true );
			if ( isset($ban) && !empty($ban) && $ban == 'true' ) {
				$customOntoSoccerSettings = get_option('custom_ontosoccer_settings', array());
				$custom_ontosoccer_login_page_id = $customOntoSoccerSettings['custom_ontosoccer_login_page_id'];
				wp_redirect( get_permalink($custom_ontosoccer_login_page_id) . '?ban=true' );
			} else {
				clean_user_cache($user_id);
				wp_clear_auth_cookie();
				wp_set_current_user($user_id);
				wp_set_auth_cookie($user_id, true, false);
				$user = get_user_by('id', $user_id);
				update_user_caches($user);
				wp_redirect( site_url() );
			}
		}
	}

	public function custom_ontosoccer_profile_edit() {
		$user_id = get_current_user_id();
		update_user_meta( $user_id, 'first_name', $_POST['first_name'] );
		update_user_meta( $user_id, 'last_name', $_POST['last_name'] );
		update_user_meta( $user_id, 'date_of_birth', $_POST['date_of_birth'] );
		update_user_meta( $user_id, 'mobile_number', $_POST['mobile_number'] );
		update_user_meta( $user_id, 'twitter_username', $_POST['twitter_username'] );
		$customOntoSoccerSettings = get_option('custom_ontosoccer_settings', array());
		$custom_ontosoccer_profile_edit_page_id = $customOntoSoccerSettings['custom_ontosoccer_profile_edit_page_id'];
		wp_redirect( get_permalink($custom_ontosoccer_profile_edit_page_id) . '?edit-success=true' );
	}

	public function custom_ontosoccer_forgot_password() {
		$user = get_user_by( 'email', $_POST['email'] );
		if ( $user ) {
			$first_name = get_user_meta( $user->ID, 'first_name', true );
			$last_name = get_user_meta( $user->ID, 'last_name', true );
			$validation_code = $this->generateRandomString();
			update_user_meta( $user->ID, 'validation_code', $validation_code );
			$customOntosoccerEmailTemplate = get_option('custom_ontosoccer_email_template', array());
			$email_template = $customOntosoccerEmailTemplate['email_template'];
			$to = $_POST['email'];
			$subject = 'Forgot Password';
			$from = get_option('admin_email');
			// To send HTML mail, the Content-type header must be set
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			// Create email headers
			$headers .= 'From: '.$from."\r\n".
				'Reply-To: '.$from."\r\n" .
				'X-Mailer: PHP/' . phpversion();
			// Compose a simple HTML email message
			$message = '';
			$message .= '<h3>Hi '.$first_name.' '.$last_name.'!</h3>';
			$customOntoSoccerSettings = get_option('custom_ontosoccer_settings', array());
			$custom_ontosoccer_validate_email_page_id = $customOntoSoccerSettings['custom_ontosoccer_validate_email_page_id'];
			$message .= '<p style="font-weight: bold;">Your password verification code is: '.$validation_code.'</p>';
			$message .= '<p>Please go to the link: <a href="'.get_permalink($custom_ontosoccer_validate_email_page_id).'?user='.$user->ID.'&password-success=true">Verify Password</a> to validate your email and then you will be able to change your password.</p>';
			$message .= '<p>Thanks for your attention!</p>';
			$message .= '';
			$email_template = str_replace("{{CODE}}", html_entity_decode(htmlentities(wpautop($message))), $email_template);
			// Sending Mail
			wp_mail($to, $subject, $email_template, $headers);
		}
		$customOntoSoccerSettings = get_option('custom_ontosoccer_settings', array());
		$custom_ontosoccer_forgot_password_page_id = $customOntoSoccerSettings['custom_ontosoccer_forgot_password_page_id'];
		wp_redirect( get_permalink($custom_ontosoccer_forgot_password_page_id) . '?password-success=true' );
	}

	public function custom_ontosoccer_contact_us() {
		$first_name = $_POST['first_name'];
		$last_name = $_POST['last_name'];
		$email_address = $_POST['email_address'];
		$mobile_number = $_POST['mobile_number'];
		$message = $_POST['message'];
		$user_message = $message;
		global $user_ID;
		$post = array(
			'post_title' => rand(1000000000, 9999999999),
			'post_content' => $message,
			'post_status' => 'publish',
			'post_date' => date('Y-m-d H:i:s'),
			'post_author' => $user_ID,
			'post_type' => 'co-contact-us'
		);
		$post_id = wp_insert_post($post);
		update_post_meta( $post_id, 'first_name', $first_name );
		update_post_meta( $post_id, 'last_name', $last_name );
		update_post_meta( $post_id, 'email_address', $email_address );
		update_post_meta( $post_id, 'mobile_number', $mobile_number );
		update_post_meta( $post_id, 'message', $message );
		$customOntosoccerEmailTemplate = get_option('custom_ontosoccer_email_template', array());
		$email_template = $customOntosoccerEmailTemplate['email_template'];
		// Send Mail to Admin
		$to = get_option('admin_email');
		$subject = 'Contact Query';
		$from = get_option('admin_email');
		// To send HTML mail, the Content-type header must be set
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		// Create email headers
		$headers .= 'From: '.$from."\r\n".
			'Reply-To: '.$from."\r\n" .
			'X-Mailer: PHP/' . phpversion();
		// Compose a simple HTML email message
		$message = '';
		$message .= '<h3>Contact Details</h3>';
		$message .= '<strong>Name: </strong>' . $first_name . ' ' . $last_name;
		$message .= '<br><strong>Email: </strong>' . $email_address;
		$message .= '<br><strong>Mobile Number: </strong>' . $mobile_number;
		$message .= '<p><strong>Message: </strong><br>' . $user_message . '</p>';
		$message .= '';
		$email_template = str_replace("{{CODE}}", html_entity_decode(htmlentities(wpautop($message))), $email_template);
		// Sending Mail
		wp_mail($to, $subject, $email_template, $headers);
		$customOntoSoccerSettings = get_option('custom_ontosoccer_settings', array());
		$custom_ontosoccer_contact_us_page_id = $customOntoSoccerSettings['custom_ontosoccer_contact_us_page_id'];
		wp_redirect( get_permalink($custom_ontosoccer_contact_us_page_id) . '?success=true' );
	}

	public function delete_custom_ontosoccer_contact_us() {
		$post_id = $_POST['post_id'];
		wp_delete_post((int) $post_id, true);
		wp_redirect( admin_url('admin.php?page=custom-ontosoccer-contact-us&delete-success=true') );
	}

	public function custom_ontosoccer_add_testimonial() {
		$display_name = $_POST['display_name'];
		$email_address = $_POST['email_address'];
		$mobile_number = $_POST['mobile_number'];
		$message = $_POST['message'];
		global $user_ID;
		$post = array(
			'post_title' => rand(1000000000, 9999999999),
			'post_content' => $message,
			'post_status' => 'publish',
			'post_date' => date('Y-m-d H:i:s'),
			'post_author' => $user_ID,
			'post_type' => 'co-testimonial'
		);
		$post_id = wp_insert_post($post);
		update_post_meta( $post_id, 'display_name', $display_name );
		update_post_meta( $post_id, 'email_address', $email_address );
		update_post_meta( $post_id, 'mobile_number', $mobile_number );
		update_post_meta( $post_id, 'message', $message );
		update_post_meta( $post_id, 'featured', 'false' );
		// Tweet to Page
		$customOntosoccerTwitterAPISettings = get_option('custom_ontosoccer_twitter_api_settings', array());
		$api_key = $customOntosoccerTwitterAPISettings['api_key'];
		$api_secret = $customOntosoccerTwitterAPISettings['api_secret'];
		$access_token = $customOntosoccerTwitterAPISettings['access_token'];
		$token_secret = $customOntosoccerTwitterAPISettings['token_secret'];
		$bearer_token = $customOntosoccerTwitterAPISettings['bearer_token'];
		$connection = new TwitterOAuth($api_key, $api_secret, $access_token, $token_secret);
		$status = $display_name . ": " . $message . "(Date: " . date('d-m-Y') . ")";
		$post_tweets = $connection->post("statuses/update", ["status" => $status]);
		$customOntoSoccerSettings = get_option('custom_ontosoccer_settings', array());
		$custom_ontosoccer_add_testimonial_page_id = $customOntoSoccerSettings['custom_ontosoccer_add_testimonial_page_id'];
		wp_redirect( get_permalink($custom_ontosoccer_add_testimonial_page_id) . '?success=true' );
	}

	public function update_custom_ontosoccer_testimonial() {
		$post_id = $_POST['post_id'];
		update_post_meta ( (int) $post_id, 'featured', $_POST['featured'] );
		wp_redirect( admin_url('admin.php?page=custom-ontosoccer-testimonial&update-success=true') );
	}

	public function delete_custom_ontosoccer_testimonial() {
		$post_id = $_POST['post_id'];
		wp_delete_post((int) $post_id, true);
		wp_redirect( admin_url('admin.php?page=custom-ontosoccer-testimonial&delete-success=true') );
	}

	public function check_subscription_email() {
		$email = $_POST['email'];
		$args = array(
			'post_type'         => 'co-subscription',
			'orderby'           => 'ID',
			'post_status'       => 'publish',
			'order'             => 'DESC',
			'posts_per_page'    => 1,
			'meta_query' => array(
				array(
					'key' => 'email_address',
					'value' => $email,
					'compare' => '=',
				)
			)
		);
		$posts = get_posts( $args );
		if (isset($posts) && is_array($posts) && count($posts) == 1) {
			echo false;
		} else {
			$email_address = $email;
			global $user_ID;
			$post = array(
				'post_title' => rand(1000000000, 9999999999),
				'post_content' => '',
				'post_status' => 'publish',
				'post_date' => date('Y-m-d H:i:s'),
				'post_author' => $user_ID,
				'post_type' => 'co-subscription'
			);
			$post_id = wp_insert_post($post);
			update_post_meta( $post_id, 'email_address', $email_address );
			update_post_meta( $post_id, 'verified', 'false' );
			$validation_code = $this->generateRandomString();
			update_post_meta( $post_id, 'validation_code', $validation_code );
			$customOntosoccerEmailTemplate = get_option('custom_ontosoccer_email_template', array());
			$email_template = $customOntosoccerEmailTemplate['email_template'];
			$to = $email_address;
			$subject = 'Validate your Email for Subscription';
			$from = get_option('admin_email');
			// To send HTML mail, the Content-type header must be set
			$headers  = 'MIME-Version: 1.0' . "\r\n";
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			// Create email headers
			$headers .= 'From: '.$from."\r\n".
				'Reply-To: '.$from."\r\n" .
				'X-Mailer: PHP/' . phpversion();
			// Compose a simple HTML email message
			$message = '';
			$message .= '<h3>Welcome to Email Verification for Subscription to our Newsletter!</h3>';
			$message .= '<p style="font-weight: bold;">Your validation code is: '.$validation_code.'</p>';
			$customOntoSoccerSettings = get_option('custom_ontosoccer_settings', array());
			$custom_ontosoccer_validate_email_page_id = $customOntoSoccerSettings['custom_ontosoccer_validate_email_page_id'];
			$message .= '<p>Please go to the link: <a href="'.get_permalink($custom_ontosoccer_validate_email_page_id).'">Validate your Email</a> to validate your email by putting the validation code.</p>';
			$message .= '<p>Thanks for your attention!</p>';
			$message .= '';
			// Sending email
			$email_template = str_replace("{{CODE}}", html_entity_decode(htmlentities(wpautop($message))), $email_template);
			// Sending Mail
			wp_mail($to, $subject, $email_template, $headers);
			echo true;
		}
		exit;
	}

	public function delete_custom_ontosoccer_subscription()	{
		$post_id = $_POST['post_id'];
		wp_delete_post((int) $post_id, true);
		wp_redirect( admin_url('admin.php?page=custom-ontosoccer-subscription&delete-success=true') );
	}

	public function delete_custom_ontosoccer_prediction() {
		$post_id = $_POST['post_id'];
		wp_delete_post((int) $post_id, true);
		wp_redirect( admin_url('admin.php?page=custom-ontosoccer-prediction&delete-success=true') );
	}

	public function check_validation_code_for_subscription() {
		$validation_code = $_POST['validation_code'];
		$args = array(
			'post_type'         => 'co-subscription',
			'orderby'           => 'ID',
			'post_status'       => 'publish',
			'order'             => 'DESC',
			'posts_per_page'    => 1,
			'meta_query' => array(
				array(
					'key' => 'validation_code',
					'value' => $validation_code,
					'compare' => '=',
				)
			)
		);
		$posts = get_posts( $args );
		if (isset($posts) && is_array($posts) && count($posts) == 1) {
			update_post_meta( $posts[0]->ID, 'verified', 'true' );
			echo true;
		} else {
			echo false;
		}
		exit;
	}

	public function custom_ontosoccer_validate_subscription() {
		$customOntoSoccerSettings = get_option('custom_ontosoccer_settings', array());
		$custom_ontosoccer_validate_email_page_id = $customOntoSoccerSettings['custom_ontosoccer_validate_email_page_id'];
		wp_redirect( get_permalink($custom_ontosoccer_validate_email_page_id) . '?subscription-success=true' );
	}


	public function send_newsletter_subscription_email() {
		$subscription_users = $_POST['subscription_users'];
		if (isset($subscription_users) && is_array($subscription_users) && count($subscription_users) > 0) {
			if (in_array('all', $subscription_users)) {
				$args = array(
					'post_type'         => 'co-subscription',
					'orderby'           => 'ID',
					'post_status'       => 'publish',
					'order'             => 'DESC',
					'posts_per_page'    => -1,
					'meta_query' => array(
						array(
							'key' => 'verified',
							'value' => 'true',
							'compare' => '=',
						)
					)
				);
				$subscribers = get_posts( $args );
				$customOntosoccerEmailTemplate = get_option('custom_ontosoccer_email_template', array());
				$email_template = $customOntosoccerEmailTemplate['email_template'];
				foreach ($subscribers as $subscriber) {
					$email_address = get_post_meta( $subscriber->ID, 'email_address', true );
					$to = $email_address;
					$subject = $_POST['subject'];
					$from = get_option('admin_email');
					// To send HTML mail, the Content-type header must be set
					$headers  = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					// Create email headers
					$headers .= 'From: '.$from."\r\n".
						'Reply-To: '.$from."\r\n" .
						'X-Mailer: PHP/' . phpversion();
					// Compose a simple HTML email message
					$message = $email_template;
					$message = str_replace("{{CODE}}", html_entity_decode(htmlentities(wpautop($_POST['email_body']))), $message);
					// Sending email
					wp_mail($to, $subject, $message, $headers);
				}
			} else {
				$customOntosoccerEmailTemplate = get_option('custom_ontosoccer_email_template', array());
				$email_template = $customOntosoccerEmailTemplate['email_template'];
				foreach ($subscription_users as $user) {
					$email_address = get_post_meta( (int) $user, 'email_address', true );
					$to = $email_address;
					$subject = $_POST['subject'];
					$from = get_option('admin_email');
					// To send HTML mail, the Content-type header must be set
					$headers  = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					// Create email headers
					$headers .= 'From: '.$from."\r\n".
						'Reply-To: '.$from."\r\n" .
						'X-Mailer: PHP/' . phpversion();
					// Compose a simple HTML email message
					$message = $email_template;
					$message = str_replace("{{CODE}}", html_entity_decode(htmlentities(wpautop($_POST['email_body']))), $message);
					// Sending email
					wp_mail($to, $subject, $message, $headers);
				}
			}
		}
		wp_redirect( admin_url('admin.php?page=custom-ontosoccer-send-newsletter-email&success=true') );
	}

	public function send_user_email() {
		$users = $_POST['users'];
		if (isset($users) && is_array($users) && count($users) > 0) {
			if (in_array('all', $users)) {
				$args = array(
					'role' => 'subscriber',
					'orderby' => 'user_nicename',
					'order' => 'ASC'
				);
				$subscribers = get_users($args);
				$customOntosoccerEmailTemplate = get_option('custom_ontosoccer_email_template', array());
				$email_template = $customOntosoccerEmailTemplate['email_template'];
				foreach ($subscribers as $subscriber) {
					$email_address = $subscriber->user_email;
					$to = $email_address;
					$subject = $_POST['subject'];
					$from = get_option('admin_email');
					// To send HTML mail, the Content-type header must be set
					$headers  = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					// Create email headers
					$headers .= 'From: '.$from."\r\n".
						'Reply-To: '.$from."\r\n" .
						'X-Mailer: PHP/' . phpversion();
					// Compose a simple HTML email message
					$message = $email_template;
					$message = str_replace("{{CODE}}", html_entity_decode(htmlentities(wpautop($_POST['email_body']))), $message);
					// Sending email
					wp_mail($to, $subject, $message, $headers);
				}
			} else {
				$customOntosoccerEmailTemplate = get_option('custom_ontosoccer_email_template', array());
				$email_template = $customOntosoccerEmailTemplate['email_template'];
				foreach ($users as $user) {
					$email_address = get_userdata( (int) $user );
					$email_address = $email_address->user_email;
					$to = $email_address;
					$subject = $_POST['subject'];
					$from = get_option('admin_email');
					// To send HTML mail, the Content-type header must be set
					$headers  = 'MIME-Version: 1.0' . "\r\n";
					$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
					// Create email headers
					$headers .= 'From: '.$from."\r\n".
						'Reply-To: '.$from."\r\n" .
						'X-Mailer: PHP/' . phpversion();
					// Compose a simple HTML email message
					$message = $email_template;
					$message = str_replace("{{CODE}}", html_entity_decode(htmlentities(wpautop($_POST['email_body']))), $message);
					// Sending email
					wp_mail($to, $subject, $message, $headers);
				}
			}
		}
		wp_redirect( admin_url('admin.php?page=custom-ontosoccer-send-user-email&success=true') );
	}

	public function send_sms() {
		$users = $_POST['users'];
		if (isset($users) && is_array($users) && count($users) > 0) {
			$customOntosoccerTwilioSettings = get_option('custom_ontosoccer_twilio_settings', array());
			$account_sid = $customOntosoccerTwilioSettings['account_sid'];
			$auth_token = $customOntosoccerTwilioSettings['auth_token'];
			$twilio_number = $customOntosoccerTwilioSettings['mobile_number'];
			if (in_array('all', $users)) {
				$args = array(
					'role' => 'subscriber',
					'orderby' => 'user_nicename',
					'order' => 'ASC'
				);
				$subscribers = get_users($args);
				foreach ($subscribers as $subscriber) {
					$mobile_number = get_user_meta( (int) $subscriber->ID, 'mobile_number', true );
					// Your Account SID and Auth Token from twilio.com/console
					// In production, these should be environment variables. E.g.:
					// $auth_token = $_ENV["TWILIO_AUTH_TOKEN"]
					// A Twilio number you own with SMS capabilities
					$client = new Client($account_sid, $auth_token);
					$client->messages->create(
						// Where to send a text message (your cell phone?)
						$mobile_number,
						array(
							'from' => $twilio_number,
							'body' => $_POST['sms_body']
						)
					);
				}
			} else {
				foreach ($users as $user) {
					$mobile_number = get_user_meta( (int) $user, 'mobile_number', true );
					// Your Account SID and Auth Token from twilio.com/console
					// In production, these should be environment variables. E.g.:
					// $auth_token = $_ENV["TWILIO_AUTH_TOKEN"]
					// A Twilio number you own with SMS capabilities
					$client = new Client($account_sid, $auth_token);
					$client->messages->create(
						// Where to send a text message (your cell phone?)
						$mobile_number,
						array(
							'from' => $twilio_number,
							'body' => $_POST['sms_body']
						)
					);
				}
			}
		}
		wp_redirect( admin_url('admin.php?page=custom-ontosoccer-send-sms&success=true') );
	}

	public function send_pin_number_to_user() {
		$users = $_POST['users'];
		$notSendUsers = [];
		if (isset($users) && is_array($users) && count($users) > 0) {
			$customOntosoccerTwilioSettings = get_option('custom_ontosoccer_twilio_settings', array());
			$account_sid = $customOntosoccerTwilioSettings['account_sid'];
			$auth_token = $customOntosoccerTwilioSettings['auth_token'];
			$twilio_number = $customOntosoccerTwilioSettings['mobile_number'];
			$args = array(
				'post_type'=> 'custom-pin',
				'orderby'    => 'ID',
				'post_status' => 'publish',
				'order'    => 'DESC',
				'posts_per_page' => -1,
				'meta_query' => array(
					array(
						'key' => 'used',
						'value' => false,
						'compare' => '=',
					)
				)
			);
			$pinNumberPosts = get_posts( $args );
			if (isset($pinNumberPosts) && is_array($pinNumberPosts) && count($pinNumberPosts) > 0) {
				if (in_array('all', $users)) {
					$args = array(
						'role' => 'subscriber',
						'orderby' => 'user_nicename',
						'order' => 'ASC'
					);
					$subscribers = get_users($args);
					foreach ($subscribers as $subscriber) {
						$mobile_number = get_user_meta( (int) $subscriber->ID, 'mobile_number', true );
						$telecom_provider = get_user_meta( (int) $subscriber->ID, 'telecom_provider', true );
						$args = array(
							'post_type'=> 'custom-pin',
							'orderby'    => 'ID',
							'post_status' => 'publish',
							'order'    => 'DESC',
							'posts_per_page' => 1,
							'meta_query' => array(
								array(
									'key' => 'used',
									'value' => false,
									'compare' => '=',
								)
							)
						);
						if (count(get_posts( $args )) > 0) {
							$pinPost = get_posts( $args )[0];
							$pin_number = get_post_meta( $pinPost->ID, 'pin_number', true );
							$pin_telecom_provider = get_post_meta( $pinPost->ID, 'telecom_provider', true );
							if ($pin_telecom_provider == $telecom_provider) {
								update_post_meta( $pinPost->ID, 'used', true );
								$sms_body = $_POST['sms_body'];
								$sms_body = str_replace('{{PinNumber}}', $pin_number, $sms_body);
								// Your Account SID and Auth Token from twilio.com/console
								// In production, these should be environment variables. E.g.:
								// $auth_token = $_ENV["TWILIO_AUTH_TOKEN"]
								// A Twilio number you own with SMS capabilities
								$client = new Client($account_sid, $auth_token);
								$client->messages->create(
									// Where to send a text message (your cell phone?)
									$mobile_number,
									array(
										'from' => $twilio_number,
										'body' => $sms_body
									)
								);
							} else {
								array_push($notSendUsers, (int) $subscriber->ID);
							}
						} else {
							array_push($notSendUsers, (int) $subscriber->ID);
						}
					}
				} else {
					foreach ($users as $user) {
						$mobile_number = get_user_meta( (int) $user, 'mobile_number', true );
						$telecom_provider = get_user_meta( (int) $user, 'telecom_provider', true );
						$args = array(
							'post_type'=> 'custom-pin',
							'orderby'    => 'ID',
							'post_status' => 'publish',
							'order'    => 'DESC',
							'posts_per_page' => 1,
							'meta_query' => array(
								array(
									'key' => 'used',
									'value' => false,
									'compare' => '=',
								)
							)
						);
						if (count(get_posts( $args )) > 0) {
							$pinPost = get_posts( $args )[0];
							$pin_number = get_post_meta( $pinPost->ID, 'pin_number', true );
							$pin_telecom_provider = get_post_meta( $pinPost->ID, 'telecom_provider', true );
							if ($pin_telecom_provider == $telecom_provider) {
								update_post_meta( $pinPost->ID, 'used', true );
								$sms_body = $_POST['sms_body'];
								$sms_body = str_replace('{{PinNumber}}', $pin_number, $sms_body);
								// Your Account SID and Auth Token from twilio.com/console
								// In production, these should be environment variables. E.g.:
								// $auth_token = $_ENV["TWILIO_AUTH_TOKEN"]
								// A Twilio number you own with SMS capabilities
								$client = new Client($account_sid, $auth_token);
								$client->messages->create(
									// Where to send a text message (your cell phone?)
									$mobile_number,
									array(
										'from' => $twilio_number,
										'body' => $sms_body
									)
								);
							} else {
								array_push($notSendUsers, (int) $user);
							}
						} else {
							array_push($notSendUsers, (int) $user);
						}
					}
				}
			} else {
				wp_redirect( admin_url('admin.php?page=custom-ontosoccer-send-pin-number-to-user&success=false') );
			}
		}
		if (count($notSendUsers) > 0) {
			$notSendUsers = implode (',', $notSendUsers);
			$notSendUsers = '&users='.$notSendUsers;
		} else {
			$notSendUsers = '';
		}
		wp_redirect( admin_url('admin.php?page=custom-ontosoccer-send-pin-number-to-user&success=true'.$notSendUsers) );
	}

	public function users_bulk_actions( $bulk_array ) {
		$bulk_array['ban_users'] = 'Ban Users';
		$bulk_array['unban_users'] = 'Unban Users';
		return $bulk_array;
	}

	public function users_bulk_action_handler( $redirect, $doaction, $object_ids ) {
		// let's remove query args first
		$redirect = remove_query_arg( array( 'ban_users', 'unban_users' ), $redirect );
		// do something for "Make Draft" bulk action
		if ( $doaction == 'ban_users' ) {
			foreach ( $object_ids as $post_id ) {
				update_user_meta( $post_id, 'ban', 'true' );
			}
			// do not forget to add query args to URL because we will show notices later
			$redirect = add_query_arg(
				'ban_users', // just a parameter for URL (we will use $_GET['misha_make_draft_done'] )
				count( $object_ids ), // parameter value - how much posts have been affected
			$redirect );
		}
		// do something for "Set price to $1000" bulk action
		if ( $doaction == 'unban_users' ) {
			foreach ( $object_ids as $post_id ) {
				update_user_meta( $post_id, 'ban', 'false' );
			}
			$redirect = add_query_arg( 'unban_users', count( $object_ids ), $redirect );
		}
		return $redirect;
	}

	public function users_bulk_actions_notices() {
		if( ! empty( $_REQUEST['ban_users'] ) ) {
			printf( '<div id="message" class="updated notice is-dismissible"><p>' .
				_n( 'Successfully banned %s users.',
				'Successfully banned %s users.',
				intval( $_REQUEST['ban_users'] )
			) . '</p></div>', intval( $_REQUEST['ban_users'] ) );
		}
		if( ! empty( $_REQUEST['unban_users'] ) ) {
			printf( '<div id="message" class="updated notice is-dismissible"><p>' .
				_n( 'Successfully unbanned %s users.',
				'Successfully unbanned %s users.',
				intval( $_REQUEST['unban_users'] )
			) . '</p></div>', intval( $_REQUEST['unban_users'] ) );
		}
	}

	public function manage_users_columns( $column ) {
		$column['ban_status'] = 'Ban Status';
		return $column;
	}

	public function manage_users_custom_column( $val, $column_name, $user_id ) {
		if ($column_name == 'ban_status') {
			$ban = get_user_meta( $user_id, 'ban', true );
			if (isset($ban) && !empty($ban) && $ban == 'true') {
				return '<span class="badge badge-danger">Banned</span>';
			} else {
				return '<span class="badge badge-success">Not Banned</span>';
			}
		}
		return $val;
	}

	public function count_file_contents() {
		$file = file($_POST['attachmentURL'], FILE_SKIP_EMPTY_LINES);
		echo count($file);
		exit;
	}

	public function import_matches_records() {
		$start = (int) $_POST['start'];
		$end = (int) $_POST['end'];
		$count = 1;
		$file = fopen($_POST['attachmentURL'], 'r');
		while (($line = fgetcsv($file)) !== FALSE) {
			if ($count >= $start && $count <= $end && isset($line[0]) && !empty($line[0])) {
				global $user_ID;
				$new_post = array(
					'post_title' => $line[0],
					'post_status' => 'publish',
					'post_date' => date('Y-m-d H:i:s'),
					'post_author' => $user_ID,
					'post_type' => 'custom-match',
				);
				$post_id = wp_insert_post($new_post);
				// Update Meta Data
				update_post_meta( $post_id, 'first_team', $line[1]);
				update_post_meta( $post_id, 'second_team', $line[2]);
				$date = str_replace('/', '-', $line[3]);
				$date = date('Y-m-d', strtotime($date));
				update_post_meta( $post_id, 'match_date', $date);
				$date = date_create( $date );
				$date = '' . date_format( $date, 'Y/m/d' );
				$week = new DateTime($date);
				$week = $week->format("W");
				update_post_meta( $post_id, 'match_date', $_POST['match_date']);
				update_post_meta( $post_id, 'match_week', $week);
				update_post_meta( $post_id, 'match_time', $line[4]);
				update_post_meta( $post_id, 'first_team_goals', '');
				update_post_meta( $post_id, 'second_team_goals', '');
			}
			$count++;
		}
		fclose($file);
		exit;
	}

	public function import_pin_numbers_records() {
		$start = (int) $_POST['start'];
		$end = (int) $_POST['end'];
		$count = 1;
		$file = fopen($_POST['attachmentURL'], 'r');
		while (($line = fgetcsv($file)) !== FALSE) {
			if ($count >= $start && $count <= $end && isset($line[0]) && !empty($line[0])) {
				global $user_ID;
				$new_post = array(
					'post_title' => $line[0],
					'post_status' => 'publish',
					'post_date' => date('Y-m-d H:i:s'),
					'post_author' => $user_ID,
					'post_type' => 'custom-pin',
				);
				$post_id = wp_insert_post($new_post);
				// Update Meta Data
				update_post_meta( $post_id, 'pin_number', $line[0]);
				update_post_meta( $post_id, 'telecom_provider', $line[1]);
				update_post_meta( $post_id, 'used', false);
			}
			$count++;
		}
		fclose($file);
		exit;
	}

	public function manage_custom_match_posts_columns( $column_array ) {
		$column_array['first_team_score'] = 'First Team Score';
		$column_array['second_team_score'] = 'Second Team Score';
		return $column_array;
	}

	public function manage_custom_match_custom_column( $column_name, $post_id ) {
		switch( $column_name ) :
			case 'first_team_score': {
				$first_team_score = get_post_meta( $post_id, 'first_team_goals', true );
				if ( isset($first_team_score) && !empty($first_team_score) ) {
					echo $first_team_score;
				} else {
					echo "Not set.";
				}
				break;
			}
			case 'second_team_score': {
				$second_team_score = get_post_meta( $post_id, 'second_team_goals', true );
				if ( isset($second_team_score) && !empty($second_team_score) ) {
					echo $second_team_score;
				} else {
					echo "Not set.";
				}
				break;
			}
		endswitch;
	}

	public function custom_quick_edit_fields( $column_name, $post_type ) {
		switch( $column_name ) :
			case 'first_team_score': {
				wp_nonce_field( 'misha_team_scores_edit_nonce', 'team_scores_nonce' );
				echo '<fieldset class="inline-edit-col-left clear">
						<div class="inline-edit-col wp-clearfix">
	 						<label class="">
								<span class="title" style="width: 10em;">First Team Score</span>
								<span class="input-text-wrap"><input type="number" name="first_team_score" value="" style="width: 80%;"></span>
							</label>
						</div>
						</fieldset>';
				break;
	 
			}
			case 'second_team_score': {
				echo '<fieldset class="inline-edit-col-left clear">
						<div class="inline-edit-col wp-clearfix">
	 						<label class="">
								<span class="title" style="width: 10em;">Second Team Score</span>
								<span class="input-text-wrap"><input type="number" name="second_team_score" value="" style="width: 80%;"></span>
							</label>
						</div>
						</fieldset>';
				break;
	 
			}
		endswitch;
	}

	public function custom_quick_edit_save( $post_id ) {
		// update the price
		if ( isset( $_POST['first_team_score'] ) ) {
			update_post_meta( $post_id, 'first_team_goals', $_POST['first_team_score'] );
		}
		// update checkbox
		if ( isset( $_POST['second_team_score'] ) ) {
			update_post_meta( $post_id, 'second_team_goals',  $_POST['second_team_score'] );
		}
	}

	public function submit_week_fixtures() {
		$week_fixture = $_POST['week_fixture'];
		$matches = $_POST['matches'];
		global $user_ID;
		$new_post = array(
			'post_title' => rand(1000000000, 9999999999),
			'post_status' => 'publish',
			'post_date' => date('Y-m-d H:i:s'),
			'post_author' => $user_ID,
			'post_type' => 'custom-prediction',
		);
		$post_id = wp_insert_post($new_post);
		// Update Meta Data
		update_post_meta( $post_id, 'week_fixture', $week_fixture );
		update_post_meta( $post_id, 'user_id', get_current_user_id() );
		update_post_meta( $post_id, 'matches', $matches);
		echo true;
		exit;
	}

}
