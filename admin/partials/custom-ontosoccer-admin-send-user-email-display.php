<?php
    $args = array(
        'role' => 'subscriber',
        'orderby' => 'user_nicename',
        'order' => 'ASC'
    );
    $subscribers = get_users($args);
?>

<div class="wrap custom-ontosoccer-send-user-email-div">
    <h2 style="margin-bottom: 1rem;"><?php _e( 'Send User Email', 'custom-ontosoccer' ); ?></h2>
    <?php if (isset($_GET['success']) && !empty($_GET['success']) && $_GET['success'] == 'true') { ?>
        <div class="notice notice-success is-dismissible">
            <p><?php _e( 'Email has been sent successfully!', 'custom-ontosoccer' ); ?></p>
        </div>
    <?php } ?>
    <form method="POST" action="<?php echo admin_url('admin-post.php'); ?>">
        <input type="hidden" name="action" value="send_user_email">
        <table class="widefat form-table custom-ontosoccer-settings-table">
            <tbody>
                <tr>
                    <td scope="row" width="150">
                        <label for="subject"><?php _e( 'Enter Subject', 'custom-ontosoccer' ); ?></label>
                    </td>
                    <td>
                        <input type="text" id="subject" name="subject" class="wd100" style="width: 100%;">
                    </td>
                </tr>
                <tr>
                    <td scope="row" width="150">
                        <label for="email_body"><?php _e( 'Enter Email', 'custom-ontosoccer' ); ?></label>
                    </td>
                    <td>
                        <?php wp_editor('', 'email_body'); ?>
                    </td>
                </tr>
                <tr>
                    <td scope="row" width="150">
                        <label for="users"><?php _e( 'Select Users', 'custom-ontosoccer' ); ?></label>
                    </td>
                    <td>
                        <select name="users[]" id="users" class="js-example-basic-multiple" style="width: 100%;" multiple required>
                            <option value="all"><?php _e( 'All Users', 'custom-ontosoccer' ); ?></option>
                            <?php foreach ($subscribers as $subscriber) { ?>
                                <option value="<?php echo $subscriber->ID; ?>"><?php echo $subscriber->user_email; ?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
        <?php submit_button('Send Email'); ?>
    </form>
</div>