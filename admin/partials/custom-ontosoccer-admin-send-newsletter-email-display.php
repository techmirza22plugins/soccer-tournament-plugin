<?php
    $args = array(
        'post_type'         => 'co-subscription',
        'orderby'           => 'ID',
        'post_status'       => 'publish',
        'order'             => 'DESC',
        'posts_per_page'    => -1,
        'meta_query' => array(
            array(
                'key' => 'verified',
                'value' => 'true',
                'compare' => '=',
            )
        )
    );
    $subscribers = get_posts( $args );
?>

<div class="wrap custom-ontosoccer-send-newsletter-email-div">
    <h2 style="margin-bottom: 1rem;"><?php _e( 'Send Newsletter Email', 'custom-ontosoccer' ); ?></h2>
    <?php if (isset($_GET['success']) && !empty($_GET['success']) && $_GET['success'] == 'true') { ?>
        <div class="notice notice-success is-dismissible">
            <p><?php _e( 'Email has been sent successfully!', 'custom-ontosoccer' ); ?></p>
        </div>
    <?php } ?>
    <form method="POST" action="<?php echo admin_url('admin-post.php'); ?>" class="send-newsletter-email">
        <input type="hidden" name="action" value="send_newsletter_subscription_email">
        <table class="widefat form-table custom-ontosoccer-settings-table">
            <tbody>
                <tr>
                    <td scope="row" width="150">
                        <label for="subject"><?php _e( 'Enter Subject', 'custom-ontosoccer' ); ?></label>
                    </td>
                    <td>
                        <input type="text" id="subject" name="subject" class="wd100" style="width: 100%;">
                    </td>
                </tr>
                <tr>
                    <td scope="row" width="150">
                        <label for="email_body"><?php _e( 'Enter Email', 'custom-ontosoccer' ); ?></label>
                    </td>
                    <td>
                        <?php wp_editor('', 'email_body'); ?>
                    </td>
                </tr>
                <tr>
                    <td scope="row" width="150">
                        <label for="subscription_users"><?php _e( 'Select Subscribers', 'custom-ontosoccer' ); ?></label>
                    </td>
                    <td>
                        <select name="subscription_users[]" id="subscription_users" class="js-example-basic-multiple" style="width: 100%;" multiple required>
                            <option value="all"><?php _e( 'All Subscribers', 'custom-ontosoccer' ); ?></option>
                            <?php foreach ($subscribers as $subscriber) { ?>
                                <option value="<?php echo $subscriber->ID; ?>"><?php echo get_post_meta( $subscriber->ID, 'email_address', true ); ?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
            </tbody>
        </table>
        <?php submit_button('Send Email'); ?>
    </form>
</div>