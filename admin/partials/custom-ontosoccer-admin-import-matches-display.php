<style>
    .wd100 {
        width: 100%;
    }
</style>
<div class="wrap custom-ontosoccer-import-matches-div">
    <h2 style="margin-bottom: 1rem;"><?php _e( 'Import Matches', 'custom-ontosoccer' ); ?></h2>
    <div id="progressbar"></div>
    <table class="widefat form-table custom-ontosoccer-settings-table" style="margin-top: 2rem;">
        <tbody>
            <tr>
                <td scope="row" width="150">
                    <label for="uploaded-csv"><?php _e( 'Uploaded File', 'custom-ontosoccer' ); ?></label>
                </td>
                <td>
                    <input type="text" id="uploaded-csv" class="wd100">
                    <input type="hidden" id="attachment-id">
                    <input type="hidden" id="attachment-url">
                </td>
                <td>
                    <button type="button" class="button button-primary upload-csv-file-btn">Upload CSV</button>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <button type="button" class="button button-primary import-matches-btn">Import Matches</button>
                </td>
            </tr>
        </tbody>
    </table>
</div>