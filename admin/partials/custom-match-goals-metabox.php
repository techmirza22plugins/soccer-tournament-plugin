<div class="wrap">
    <table class="form-table">
        <tbody id="custom-ontosoccer-match-data">
            <tr>
                <td class="first-column"><label for="first_team_goals"><?php _e( "First Team Goals", "roster-automation" ); ?></label></td>
                <td>
                    <input type="number" name="first_team_goals" id="first_team_goals" class="wd100" required="required" placeholder="Enter First Team Goals" value="<?php echo get_post_meta($post->ID,"first_team_goals",true); ?>" />
                </td>
            </tr>
            <tr>
                <td><label for="second_team_goals"><?php _e( "Second Team Goals", "roster-automation" ); ?></label></td>
                <td>
                    <input type="number" name="second_team_goals" id="second_team_goals" class="wd100" required="required" placeholder="Enter Second Team Goals" value="<?php echo get_post_meta($post->ID,"second_team_goals",true); ?>" />
                </td>
            </tr>
            <tr>
        </tbody>
    </table>
</div>