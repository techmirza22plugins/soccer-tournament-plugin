<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://wppb.me/
 * @since      1.0.0
 *
 * @package    Custom_Ontosoccer
 * @subpackage Custom_Ontosoccer/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<?php
    $args = array(
        'post_type'         => 'co-contact-us',
        'orderby'           => 'ID',
        'post_status'       => 'publish',
        'order'             => 'DESC',
        'posts_per_page'    => -1
    );
    $posts = get_posts( $args );
?>

<div class="wrap custom-ontosoccer-contact-us-div">
    <h2 style="margin-bottom: 1rem;"><?php _e( 'Contact Us Entries', 'custom-ontosoccer' ); ?></h2>
    <?php if (isset($_GET['delete-success']) && !empty($_GET['delete-success']) && $_GET['delete-success'] == 'true') { ?>
        <div class="notice notice-success is-dismissible">
            <p><?php _e( 'Entry has been deleted successfully!', 'custom-ontosoccer' ); ?></p>
        </div>
    <?php } ?>
    <table id="example" class="display datatable contact-us-datatable" style="width:100%;">
        <thead>
            <tr>
                <th>Name</th>
                <th>Email Address</th>
                <th>Phone Number</th>
                <th>Message</th>
                <th>Date</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($posts as $post) { ?>
                <tr>
                    <td><?php echo get_post_meta( $post->ID, 'first_name', true ) . ' ' . get_post_meta( $post->ID, 'last_name', true ); ?></td>
                    <td><?php echo get_post_meta( $post->ID, 'email_address', true ); ?></td>
                    <td><?php echo get_post_meta( $post->ID, 'mobile_number', true ); ?></td>
                    <td><?php echo get_post_meta( $post->ID, 'message', true ); ?></td>
                    <td><?php echo date('d/m/Y', strtotime($post->post_date)); ?></td>
                    <td>
                        <form action="<?php echo admin_url('admin-post.php'); ?>" method="POST">
                            <input type="hidden" name="action" value="delete_custom_ontosoccer_contact_us">
                            <input type="hidden" name="post_id" value="<?php echo $post->ID; ?>">
                            <button class="button button-primary" type="submit">Delete</button>
                        </form>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
        <tfoot>
            <tr>
                <th>Name</th>
                <th>Email Address</th>
                <th>Phone Number</th>
                <th>Message</th>
                <th>Date</th>
                <th>Actions</th>
            </tr>
        </tfoot>
    </table>
</div>
