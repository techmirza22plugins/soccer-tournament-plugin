<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://wppb.me/
 * @since      1.0.0
 *
 * @package    Custom_Ontosoccer
 * @subpackage Custom_Ontosoccer/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<?php
    $args = array(
        'post_type'         => 'co-subscription',
        'orderby'           => 'ID',
        'post_status'       => 'publish',
        'order'             => 'DESC',
        'posts_per_page'    => -1
    );
    $posts = get_posts( $args );
?>

<style>
    .badge {
        display: inline-block;
        padding: .5em .5em;
        font-size: 75%;
        font-weight: 700;
        line-height: 1;
        text-align: center;
        white-space: nowrap;
        vertical-align: baseline;
        border-radius: .25rem;
        transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    }
    .badge-success {
        color: #fff;
        background-color: #28a745;
    }
    .badge-danger {
        color: #fff;
        background-color: #dc3545;
    }
</style>

<div class="wrap custom-ontosoccer-subscription-div">
    <h2 style="margin-bottom: 1rem;"><?php _e( 'Subscription Entries', 'custom-ontosoccer' ); ?></h2>
    <?php if (isset($_GET['delete-success']) && !empty($_GET['delete-success']) && $_GET['delete-success'] == 'true') { ?>
        <div class="notice notice-success is-dismissible">
            <p><?php _e( 'Entry has been deleted successfully!', 'custom-ontosoccer' ); ?></p>
        </div>
    <?php } ?>
    <table id="example" class="display datatable subscription-datatable" style="width:100%;">
        <thead>
            <tr>
                <th>#</th>
                <th>Email Address</th>
                <th>Verified</th>
                <th>Date</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php $count = 1; foreach ($posts as $post) { ?>
                <tr>
                    <td><?php echo $count; ?></td>
                    <td><?php echo get_post_meta( $post->ID, 'email_address', true ); ?></td>
                    <?php
                        $verified = get_post_meta( $post->ID, 'verified', true );
                        if (isset($verified) && !empty($verified) && $verified == 'true') { ?>
                            <td><span class="badge badge-success">Verified</span></td>
                        <?php } else { ?>
                            <td><span class="badge badge-danger">Not Verified</span></td>
                        <?php }
                    ?>
                    <td><?php echo date('d/m/Y', strtotime($post->post_date)); ?></td>
                    <td>
                        <form action="<?php echo admin_url('admin-post.php'); ?>" method="POST">
                            <input type="hidden" name="action" value="delete_custom_ontosoccer_subscription">
                            <input type="hidden" name="post_id" value="<?php echo $post->ID; ?>">
                            <button class="button button-primary" type="submit">Delete</button>
                        </form>
                    </td>
                </tr>
            <?php $count++; } ?>
        </tbody>
        <tfoot>
            <tr>
                <th>#</th>
                <th>Email Address</th>
                <th>Verified</th>
                <th>Date</th>
                <th>Actions</th>
            </tr>
        </tfoot>
    </table>
</div>
