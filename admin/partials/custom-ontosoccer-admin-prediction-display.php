<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://wppb.me/
 * @since      1.0.0
 *
 * @package    Custom_Ontosoccer
 * @subpackage Custom_Ontosoccer/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<?php
    $args = array(
        'post_type'         => 'custom-prediction',
        'orderby'           => 'ID',
        'post_status'       => 'publish',
        'order'             => 'DESC',
        'posts_per_page'    => -1
    );
    $posts = get_posts( $args );
?>

<style>
    td.details-control {
        background: url('https://datatables.net/examples/resources/details_open.png') no-repeat center center;
        cursor: pointer;
    }
    tr.shown td.details-control {
        background: url('https://datatables.net/examples/resources/details_close.png') no-repeat center center;
    }
</style>

<div class="wrap custom-ontosoccer-prediction-div">
    <h2 style="margin-bottom: 1rem;"><?php _e( 'Prediction Entries', 'custom-ontosoccer' ); ?></h2>
    <?php if (isset($_GET['delete-success']) && !empty($_GET['delete-success']) && $_GET['delete-success'] == 'true') { ?>
        <div class="notice notice-success is-dismissible">
            <p><?php _e( 'Entry has been deleted successfully!', 'custom-ontosoccer' ); ?></p>
        </div>
    <?php } ?>
    <table id="example" class="display datatable prediction-datatable" style="width:100%;">
        <thead>
            <tr>
                <th></th>
                <th>#</th>
                <th>Username</th>
                <th>User Email</th>
                <th>Week</th>
                <th>Predicted Matches</th>
                <th>Date</th>
                <th>Actions</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <?php $count = 1; foreach ($posts as $post) {
                $user_id = get_post_meta( $post->ID, 'user_id', true );
                $matches = get_post_meta( $post->ID, 'matches', true );
                for ($i = 0; $i < count($matches); $i++) {
                    $matchID = $matches[$i]['ID'];
                    $matches[$i]['firstTeam'] = get_post_meta( (int) $matchID, 'first_team', true );
                    $matches[$i]['secondTeam'] = get_post_meta( (int) $matchID, 'second_team', true );
                }
                $week_fixture = get_post_meta( $post->ID, 'week_fixture', true );
                $user_info = get_userdata($user_id);
                $first_name = get_user_meta( (int) $user_id, 'first_name', true );
                $last_name = get_user_meta( (int) $user_id, 'last_name', true );
                ?>
                <tr>
                    <td></td>
                    <td><?php echo $count; ?></td>
                    <td><?php echo $first_name . ' ' . $last_name; ?></td>
                    <td><?php echo $user_info->user_email; ?></td>
                    <td>Week <?php echo $week_fixture; ?></td>
                    <td><?php echo count($matches); ?></td>
                    <td><?php echo date('d/m/Y H:i A', strtotime($post->post_date)); ?></td>
                    <td>
                        <form action="<?php echo admin_url('admin-post.php'); ?>" method="POST">
                            <input type="hidden" name="action" value="delete_custom_ontosoccer_prediction">
                            <input type="hidden" name="post_id" value="<?php echo $post->ID; ?>">
                            <button class="button button-primary" type="submit">Delete</button>
                        </form>
                    </td>
                    <td><?php echo json_encode($matches); ?></td>
                </tr>
            <?php $count++; } ?>
        </tbody>
        <tfoot>
            <tr>
                <th></th>
                <th>#</th>
                <th>Username</th>
                <th>User Email</th>
                <th>Week</th>
                <th>Predicted Matches</th>
                <th>Date</th>
                <th>Actions</th>
                <th></th>
            </tr>
        </tfoot>
    </table>
</div>
