<?php
    $args = array(
        'role' => 'subscriber',
        'orderby' => 'user_nicename',
        'order' => 'ASC'
    );
    $subscribers = get_users($args);
?>

<div class="wrap custom-ontosoccer-send-pin-number-to-user-div">
    <h2 style="margin-bottom: 1rem;"><?php _e( 'Send Pin Number to User', 'custom-ontosoccer' ); ?></h2>
    <?php if (isset($_GET['success']) && !empty($_GET['success']) && $_GET['success'] == 'true') { ?>
        <div class="notice notice-success is-dismissible">
            <p><?php _e( 'Pin Number SMS has been sent successfully!', 'custom-ontosoccer' ); ?></p>
        </div>
    <?php } ?>
    <?php if (isset($_GET['users']) && !empty($_GET['users'])) {
        $users = explode (",", $_GET['users']); ?>
        <div class="notice notice-error is-dismissible">
            <p><?php _e( 'These users are not notified due to shortage of pin numbers.', 'custom-ontosoccer' ); ?></p>
            <?php foreach ($users as $user) { ?>
                <p><?php echo get_user_meta( (int) $user, 'first_name', true ) . ' ' . get_user_meta( (int) $user, 'last_name', true ); ?></p>
            <?php } ?>
        </div>
    <?php } ?>
    <form method="POST" action="<?php echo admin_url('admin-post.php'); ?>">
        <input type="hidden" name="action" value="send_pin_number_to_user">
        <table class="widefat form-table custom-ontosoccer-settings-table">
            <tbody>
                <tr>
                    <td scope="row" width="150">
                        <label for="users"><?php _e( 'Select Users', 'custom-ontosoccer' ); ?></label>
                    </td>
                    <td>
                        <select name="users[]" id="users" class="select2-sms-users" style="width: 100%;" multiple required>
                            <option value=""><?php _e( 'Select User', 'custom-ontosoccer' ); ?></option>
                            <?php foreach ($subscribers as $subscriber) { ?>
                                <option value="<?php echo $subscriber->ID; ?>"><?php echo $subscriber->user_email; ?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td scope="row" width="150"></td>
                    <td>
                        <div class="alert-info">
                            Please insert {{PinNumber}} at any place in message to place the pin number.
                        </div>
                    </td>
                </tr>
                <tr>
                    <td scope="row" width="150">
                        <label for="sms_body"><?php _e( 'Enter SMS', 'custom-ontosoccer' ); ?></label>
                    </td>
                    <td>
                        <textarea name="sms_body" id="sms_body" cols="30" rows="10" style="width: 100%;"></textarea>
                    </td>
                </tr>
            </tbody>
        </table>
        <?php submit_button('Send SMS'); ?>
    </form>
</div>