<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://wppb.me/
 * @since      1.0.0
 *
 * @package    Custom_Ontosoccer
 * @subpackage Custom_Ontosoccer/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<?php
    $tab = (isset($_GET['tab']) && !empty($_GET['tab'])) ? $_GET['tab'] : "instructions";
?>

<div class="wrap custom-ontosoccer-settings-div">
    <h2><?php _e( 'Custom OntoSoccer Settings', 'custom-ontosoccer' ); ?></h2>
    <?php settings_errors(); ?>
    <h2 class="nav-tab-wrapper">
        <a href="<?php echo admin_url('admin.php?page=custom-ontosoccer-settings&tab=instructions'); ?>" class="nav-tab <?php echo ($tab=='instructions') ? 'nav-tab-active' : ''; ?>">
            <?php _e( 'Instructions', 'custom-ontosoccer' ); ?>
        </a>
        <a href="<?php echo admin_url('admin.php?page=custom-ontosoccer-settings&tab=pages-settings'); ?>" class="nav-tab <?php echo ($tab=='pages-settings') ? 'nav-tab-active' : ''; ?>">
            <?php _e( 'Pages Settings', 'custom-ontosoccer' ); ?>
        </a>
        <a href="<?php echo admin_url('admin.php?page=custom-ontosoccer-settings&tab=users-email-settings'); ?>" class="nav-tab <?php echo ($tab=='users-email-settings') ? 'nav-tab-active' : ''; ?>">
            <?php _e( 'Email Template Settings', 'custom-ontosoccer' ); ?>
        </a>
        <a href="<?php echo admin_url('admin.php?page=custom-ontosoccer-settings&tab=twilio-settings'); ?>" class="nav-tab <?php echo ($tab=='twilio-settings') ? 'nav-tab-active' : ''; ?>">
            <?php _e( 'Twilio Settings', 'custom-ontosoccer' ); ?>
        </a>
        <a href="<?php echo admin_url('admin.php?page=custom-ontosoccer-settings&tab=twitter-api-settings'); ?>" class="nav-tab <?php echo ($tab=='twitter-api-settings') ? 'nav-tab-active' : ''; ?>">
            <?php _e( 'Twitter API Settings', 'custom-ontosoccer' ); ?>
        </a>
    </h2>
    <?php if ($tab == 'instructions') { ?>
        <ul>
            <li>Following is the list of shortcode for Custom OntoSoccer which can be easily integrated into the pages.</li>
            <li>1. <code>[custom-ontosoccer-registration]</code> can be placed anywhere in the page to load <strong>Registration Page</strong>.</li>
            <li>2. <code>[custom-ontosoccer-login]</code> can be placed anywhere in the page to load <strong>Login Page</strong>.</li>
            <li>3. <code>[custom-ontosoccer-validate-email]</code> can be placed anywhere in the page to load <strong>Validate Email Page</strong>.</li>
            <li>4. <code>[custom-ontosoccer-change-password]</code> can be placed anywhere in the page to load <strong>Change Password Page</strong>.</li>
            <li>5. <code>[custom-ontosoccer-edit-profile]</code> can be placed anywhere in the page to load <strong>Edit Profile Page</strong>.</li>
            <li>6. <code>[custom-ontosoccer-forgot-password]</code> can be placed anywhere in the page to load <strong>Forgot Password Page</strong>.</li>
            <li>8. <code>[custom-ontosoccer-contact-us]</code> can be placed anywhere in the page to load <strong>Contact Us Page</strong>.</li>
            <li>9. <code>[custom-ontosoccer-add-testimonial]</code> can be placed anywhere in the page to load <strong>Add Testimonial Page</strong>.</li>
            <li>10. <code>[custom-ontosoccer-list-testimonials]</code> can be placed anywhere in the page to load <strong>List Testimonials Page</strong>.</li>
            <li>11. <code>[custom-ontosoccer-week-fixtures]</code> can be placed anywhere in the page to load <strong>List Week Fixtures</strong>.</li>
            <li>12. <code>[custom-ontosoccer-week-fixtures-standings]</code> can be placed anywhere in the page to load <strong>List Week Fixtures Winners/Standings</strong>.</li>
        </ul>
    <?php } ?>
    <?php if ($tab == 'pages-settings') { ?>
        <form method="POST" action="options.php">
            <?php settings_fields( 'custom-ontosoccer-settings' );
            do_settings_sections( 'custom-ontosoccer-settings' );
            $customOntoSoccerSettings = get_option('custom_ontosoccer_settings', array());
            $custom_ontosoccer_registration_page_id = $customOntoSoccerSettings['custom_ontosoccer_registration_page_id'];
            $custom_ontosoccer_login_page_id = $customOntoSoccerSettings['custom_ontosoccer_login_page_id'];
            $custom_ontosoccer_validate_email_page_id = $customOntoSoccerSettings['custom_ontosoccer_validate_email_page_id'];
            $custom_ontosoccer_change_password_page_id = $customOntoSoccerSettings['custom_ontosoccer_change_password_page_id'];
            $custom_ontosoccer_profile_edit_page_id = $customOntoSoccerSettings['custom_ontosoccer_profile_edit_page_id'];
            $custom_ontosoccer_forgot_password_page_id = $customOntoSoccerSettings['custom_ontosoccer_forgot_password_page_id'];
            $custom_ontosoccer_contact_us_page_id = $customOntoSoccerSettings['custom_ontosoccer_contact_us_page_id'];
            $custom_ontosoccer_add_testimonial_page_id = $customOntoSoccerSettings['custom_ontosoccer_add_testimonial_page_id'];
            $custom_ontosoccer_list_testimonials_page_id = $customOntoSoccerSettings['custom_ontosoccer_list_testimonials_page_id']; ?>
            <table class="widefat form-table custom-ontosoccer-settings-table">
                <tbody>
                    <tr>
                        <td scope="row" width="150">
                            <label for="custom_ontosoccer_registration_page_id"><?php _e( 'Registration Page', 'custom-ontosoccer' ); ?></label>
                        </td>
                        <td>
                            <select name="custom_ontosoccer_settings[custom_ontosoccer_registration_page_id]" id="custom_ontosoccer_registration_page_id" class="js-example-basic-single" required>
                                <option value=""><?php _e( 'Select Page', 'custom-ontosoccer' ); ?></option>
                                <?php foreach (get_pages() as $page) { ?>
                                    <option value="<?php echo $page->ID; ?>" <?php echo (isset($custom_ontosoccer_registration_page_id) && !empty($custom_ontosoccer_registration_page_id) && $custom_ontosoccer_registration_page_id == $page->ID) ? 'selected' : ''; ?>><?php echo $page->post_title; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td scope="row" width="150">
                            <label for="custom_ontosoccer_login_page_id"><?php _e( 'Login Page', 'custom-ontosoccer' ); ?></label>
                        </td>
                        <td>
                            <select name="custom_ontosoccer_settings[custom_ontosoccer_login_page_id]" id="custom_ontosoccer_login_page_id" class="js-example-basic-single" required>
                                <option value=""><?php _e( 'Select Page', 'custom-ontosoccer' ); ?></option>
                                <?php foreach (get_pages() as $page) { ?>
                                    <option value="<?php echo $page->ID; ?>" <?php echo (isset($custom_ontosoccer_login_page_id) && !empty($custom_ontosoccer_login_page_id) && $custom_ontosoccer_login_page_id == $page->ID) ? 'selected' : ''; ?>><?php echo $page->post_title; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td scope="row" width="150">
                            <label for="custom_ontosoccer_validate_email_page_id"><?php _e( 'Validate Email Page', 'custom-ontosoccer' ); ?></label>
                        </td>
                        <td>
                            <select name="custom_ontosoccer_settings[custom_ontosoccer_validate_email_page_id]" id="custom_ontosoccer_validate_email_page_id" class="js-example-basic-single" required>
                                <option value=""><?php _e( 'Select Page', 'custom-ontosoccer' ); ?></option>
                                <?php foreach (get_pages() as $page) { ?>
                                    <option value="<?php echo $page->ID; ?>" <?php echo (isset($custom_ontosoccer_validate_email_page_id) && !empty($custom_ontosoccer_validate_email_page_id) && $custom_ontosoccer_validate_email_page_id == $page->ID) ? 'selected' : ''; ?>><?php echo $page->post_title; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td scope="row" width="150">
                            <label for="custom_ontosoccer_change_password_page_id"><?php _e( 'Change Password Page', 'custom-ontosoccer' ); ?></label>
                        </td>
                        <td>
                            <select name="custom_ontosoccer_settings[custom_ontosoccer_change_password_page_id]" id="custom_ontosoccer_change_password_page_id" class="js-example-basic-single" required>
                                <option value=""><?php _e( 'Select Page', 'custom-ontosoccer' ); ?></option>
                                <?php foreach (get_pages() as $page) { ?>
                                    <option value="<?php echo $page->ID; ?>" <?php echo (isset($custom_ontosoccer_change_password_page_id) && !empty($custom_ontosoccer_change_password_page_id) && $custom_ontosoccer_change_password_page_id == $page->ID) ? 'selected' : ''; ?>><?php echo $page->post_title; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td scope="row" width="150">
                            <label for="custom_ontosoccer_profile_edit_page_id"><?php _e( 'Profile Edit Page', 'custom-ontosoccer' ); ?></label>
                        </td>
                        <td>
                            <select name="custom_ontosoccer_settings[custom_ontosoccer_profile_edit_page_id]" id="custom_ontosoccer_profile_edit_page_id" class="js-example-basic-single" required>
                                <option value=""><?php _e( 'Select Page', 'custom-ontosoccer' ); ?></option>
                                <?php foreach (get_pages() as $page) { ?>
                                    <option value="<?php echo $page->ID; ?>" <?php echo (isset($custom_ontosoccer_profile_edit_page_id) && !empty($custom_ontosoccer_profile_edit_page_id) && $custom_ontosoccer_profile_edit_page_id == $page->ID) ? 'selected' : ''; ?>><?php echo $page->post_title; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td scope="row" width="150">
                            <label for="custom_ontosoccer_forgot_password_page_id"><?php _e( 'Forgot Password Page', 'custom-ontosoccer' ); ?></label>
                        </td>
                        <td>
                            <select name="custom_ontosoccer_settings[custom_ontosoccer_forgot_password_page_id]" id="custom_ontosoccer_forgot_password_page_id" class="js-example-basic-single" required>
                                <option value=""><?php _e( 'Select Page', 'custom-ontosoccer' ); ?></option>
                                <?php foreach (get_pages() as $page) { ?>
                                    <option value="<?php echo $page->ID; ?>" <?php echo (isset($custom_ontosoccer_forgot_password_page_id) && !empty($custom_ontosoccer_forgot_password_page_id) && $custom_ontosoccer_forgot_password_page_id == $page->ID) ? 'selected' : ''; ?>><?php echo $page->post_title; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td scope="row" width="150">
                            <label for="custom_ontosoccer_contact_us_page_id"><?php _e( 'Contact Us Page', 'custom-ontosoccer' ); ?></label>
                        </td>
                        <td>
                            <select name="custom_ontosoccer_settings[custom_ontosoccer_contact_us_page_id]" id="custom_ontosoccer_contact_us_page_id" class="js-example-basic-single" required>
                                <option value=""><?php _e( 'Select Page', 'custom-ontosoccer' ); ?></option>
                                <?php foreach (get_pages() as $page) { ?>
                                    <option value="<?php echo $page->ID; ?>" <?php echo (isset($custom_ontosoccer_contact_us_page_id) && !empty($custom_ontosoccer_contact_us_page_id) && $custom_ontosoccer_contact_us_page_id == $page->ID) ? 'selected' : ''; ?>><?php echo $page->post_title; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td scope="row" width="150">
                            <label for="custom_ontosoccer_add_testimonial_page_id"><?php _e( 'Add Testimonial Page', 'custom-ontosoccer' ); ?></label>
                        </td>
                        <td>
                            <select name="custom_ontosoccer_settings[custom_ontosoccer_add_testimonial_page_id]" id="custom_ontosoccer_add_testimonial_page_id" class="js-example-basic-single" required>
                                <option value=""><?php _e( 'Select Page', 'custom-ontosoccer' ); ?></option>
                                <?php foreach (get_pages() as $page) { ?>
                                    <option value="<?php echo $page->ID; ?>" <?php echo (isset($custom_ontosoccer_add_testimonial_page_id) && !empty($custom_ontosoccer_add_testimonial_page_id) && $custom_ontosoccer_add_testimonial_page_id == $page->ID) ? 'selected' : ''; ?>><?php echo $page->post_title; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td scope="row" width="150">
                            <label for="custom_ontosoccer_list_testimonials_page_id"><?php _e( 'List Testimonial Page', 'custom-ontosoccer' ); ?></label>
                        </td>
                        <td>
                            <select name="custom_ontosoccer_settings[custom_ontosoccer_list_testimonials_page_id]" id="custom_ontosoccer_list_testimonials_page_id" class="js-example-basic-single" required>
                                <option value=""><?php _e( 'Select Page', 'custom-ontosoccer' ); ?></option>
                                <?php foreach (get_pages() as $page) { ?>
                                    <option value="<?php echo $page->ID; ?>" <?php echo (isset($custom_ontosoccer_list_testimonials_page_id) && !empty($custom_ontosoccer_list_testimonials_page_id) && $custom_ontosoccer_list_testimonials_page_id == $page->ID) ? 'selected' : ''; ?>><?php echo $page->post_title; ?></option>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                </tbody>
            </table>
            <?php submit_button(); ?>
        </form>
    <?php } ?>
    <?php if ($tab == 'users-email-settings') { ?>
        <form method="POST" action="options.php">
            <?php settings_fields( 'custom-ontosoccer-email-template' );
            do_settings_sections( 'custom-ontosoccer-settings' );
            $customOntosoccerEmailTemplate = get_option('custom_ontosoccer_email_template', array()); ?>
            <table class="widefat form-table custom-ontosoccer-settings-table">
                <tbody>
                    <tr>
                        <td scope="row" width="150">
                            <label for="email_template"><?php _e( 'Email Template', 'custom-ontosoccer' ); ?></label>
                        </td>
                        <td>
                            <textarea name="custom_ontosoccer_email_template[email_template]" style="width: 100%"; id="email_template" class="wd100" cols="30" rows="25"><?php echo (isset($customOntosoccerEmailTemplate['email_template']) && !empty($customOntosoccerEmailTemplate['email_template'])) ? $customOntosoccerEmailTemplate['email_template'] : ''; ?></textarea>
                        </td>
                    </tr>
                </tbody>
            </table>
            <?php submit_button(); ?>
        </form>
    <?php } ?>
    <?php if ($tab == 'twilio-settings') { ?>
        <form method="POST" action="options.php">
            <?php settings_fields( 'custom-ontosoccer-twilio-settings' );
            do_settings_sections( 'custom-ontosoccer-settings' );
            $customOntosoccerTwilioSettings = get_option('custom_ontosoccer_twilio_settings', array()); ?>
            <table class="widefat form-table custom-ontosoccer-settings-table">
                <tbody>
                    <tr>
                        <td scope="row" width="150">
                            <label for="account_sid"><?php _e( 'Account SID', 'custom-ontosoccer' ); ?></label>
                        </td>
                        <td>
                            <input type="text" name="custom_ontosoccer_twilio_settings[account_sid]" style="width: 100%"; id="account_sid" class="wd100" value="<?php echo (isset($customOntosoccerTwilioSettings['account_sid']) && !empty($customOntosoccerTwilioSettings['account_sid'])) ? $customOntosoccerTwilioSettings['account_sid'] : ''; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td scope="row" width="150">
                            <label for="auth_token"><?php _e( 'Auth Token', 'custom-ontosoccer' ); ?></label>
                        </td>
                        <td>
                            <input type="text" name="custom_ontosoccer_twilio_settings[auth_token]" style="width: 100%"; id="auth_token" class="wd100" value="<?php echo (isset($customOntosoccerTwilioSettings['auth_token']) && !empty($customOntosoccerTwilioSettings['auth_token'])) ? $customOntosoccerTwilioSettings['auth_token'] : ''; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td scope="row" width="150">
                            <label for="mobile_number"><?php _e( 'Mobile Number', 'custom-ontosoccer' ); ?></label>
                        </td>
                        <td>
                            <input type="text" name="custom_ontosoccer_twilio_settings[mobile_number]" style="width: 100%"; id="mobiel_number" class="wd100" value="<?php echo (isset($customOntosoccerTwilioSettings['mobile_number']) && !empty($customOntosoccerTwilioSettings['mobile_number'])) ? $customOntosoccerTwilioSettings['mobile_number'] : ''; ?>">
                        </td>
                    </tr>
                </tbody>
            </table>
            <?php submit_button(); ?>
        </form>
    <?php } ?>
    <?php if ($tab == 'twitter-api-settings') { ?>
        <form method="POST" action="options.php">
            <?php settings_fields( 'custom-ontosoccer-twitter-api-settings' );
            do_settings_sections( 'custom-ontosoccer-settings' );
            $customOntosoccerTwitterAPISettings = get_option('custom_ontosoccer_twitter_api_settings', array()); ?>
            <table class="widefat form-table custom-ontosoccer-settings-table">
                <tbody>
                    <tr>
                        <td scope="row" width="150">
                            <label for="api_key"><?php _e( 'API Key', 'custom-ontosoccer' ); ?></label>
                        </td>
                        <td>
                            <input type="text" name="custom_ontosoccer_twitter_api_settings[api_key]" style="width: 100%"; id="api_key" class="wd100" value="<?php echo (isset($customOntosoccerTwitterAPISettings['api_key']) && !empty($customOntosoccerTwitterAPISettings['api_key'])) ? $customOntosoccerTwitterAPISettings['api_key'] : ''; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td scope="row" width="150">
                            <label for="api_secret"><?php _e( 'API Secret', 'custom-ontosoccer' ); ?></label>
                        </td>
                        <td>
                            <input type="text" name="custom_ontosoccer_twitter_api_settings[api_secret]" style="width: 100%"; id="api_secret" class="wd100" value="<?php echo (isset($customOntosoccerTwitterAPISettings['api_secret']) && !empty($customOntosoccerTwitterAPISettings['api_secret'])) ? $customOntosoccerTwitterAPISettings['api_secret'] : ''; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td scope="row" width="150">
                            <label for="access_token"><?php _e( 'Access Token', 'custom-ontosoccer' ); ?></label>
                        </td>
                        <td>
                            <input type="text" name="custom_ontosoccer_twitter_api_settings[access_token]" style="width: 100%"; id="mobiel_number" class="wd100" value="<?php echo (isset($customOntosoccerTwitterAPISettings['access_token']) && !empty($customOntosoccerTwitterAPISettings['access_token'])) ? $customOntosoccerTwitterAPISettings['access_token'] : ''; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td scope="row" width="150">
                            <label for="token_secret"><?php _e( 'Token Secret', 'custom-ontosoccer' ); ?></label>
                        </td>
                        <td>
                            <input type="text" name="custom_ontosoccer_twitter_api_settings[token_secret]" style="width: 100%"; id="mobiel_number" class="wd100" value="<?php echo (isset($customOntosoccerTwitterAPISettings['token_secret']) && !empty($customOntosoccerTwitterAPISettings['token_secret'])) ? $customOntosoccerTwitterAPISettings['token_secret'] : ''; ?>">
                        </td>
                    </tr>
                    <tr>
                        <td scope="row" width="150">
                            <label for="bearer_token"><?php _e( 'Bearer Token', 'custom-ontosoccer' ); ?></label>
                        </td>
                        <td>
                            <input type="text" name="custom_ontosoccer_twitter_api_settings[bearer_token]" style="width: 100%"; id="mobiel_number" class="wd100" value="<?php echo (isset($customOntosoccerTwitterAPISettings['bearer_token']) && !empty($customOntosoccerTwitterAPISettings['bearer_token'])) ? $customOntosoccerTwitterAPISettings['bearer_token'] : ''; ?>">
                        </td>
                    </tr>
                </tbody>
            </table>
            <?php submit_button(); ?>
        </form>
    <?php } ?>
</div>
