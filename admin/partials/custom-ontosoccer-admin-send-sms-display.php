<?php
    $args = array(
        'role' => 'subscriber',
        'orderby' => 'user_nicename',
        'order' => 'ASC'
    );
    $subscribers = get_users($args);
?>

<div class="wrap custom-ontosoccer-send-user-email-div">
    <h2 style="margin-bottom: 1rem;"><?php _e( 'Send SMS to Users', 'custom-ontosoccer' ); ?></h2>
    <?php if (isset($_GET['success']) && !empty($_GET['success']) && $_GET['success'] == 'true') { ?>
        <div class="notice notice-success is-dismissible">
            <p><?php _e( 'SMS has been sent successfully!', 'custom-ontosoccer' ); ?></p>
        </div>
    <?php } ?>
    <form method="POST" action="<?php echo admin_url('admin-post.php'); ?>">
        <input type="hidden" name="action" value="send_sms">
        <table class="widefat form-table custom-ontosoccer-settings-table">
            <tbody>
                <tr>
                    <td scope="row" width="150">
                        <label for="users"><?php _e( 'Select Users', 'custom-ontosoccer' ); ?></label>
                    </td>
                    <td>
                        <select name="users[]" id="users" class="select2-sms-users" style="width: 100%;" multiple required>
                            <option value="all"><?php _e( 'All Users', 'custom-ontosoccer' ); ?></option>
                            <?php foreach ($subscribers as $subscriber) { ?>
                                <option value="<?php echo $subscriber->ID; ?>"><?php echo $subscriber->user_email; ?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td scope="row" width="150">
                        <label for="sms_body"><?php _e( 'Enter SMS', 'custom-ontosoccer' ); ?></label>
                    </td>
                    <td>
                        <textarea name="sms_body" id="sms_body" cols="30" rows="10" style="width: 100%;"></textarea>
                    </td>
                </tr>
            </tbody>
        </table>
        <?php submit_button('Send SMS'); ?>
    </form>
</div>