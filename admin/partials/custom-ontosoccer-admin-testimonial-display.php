<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://wppb.me/
 * @since      1.0.0
 *
 * @package    Custom_Ontosoccer
 * @subpackage Custom_Ontosoccer/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<?php
    $args = array(
        'post_type'         => 'co-testimonial',
        'orderby'           => 'ID',
        'post_status'       => 'publish',
        'order'             => 'DESC',
        'posts_per_page'    => -1
    );
    $posts = get_posts( $args );
?>

<style>
    .badge {
        display: inline-block;
        padding: .5em .5em;
        font-size: 75%;
        font-weight: 700;
        line-height: 1;
        text-align: center;
        white-space: nowrap;
        vertical-align: baseline;
        border-radius: .25rem;
        transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
    }
    .badge-success {
        color: #fff;
        background-color: #28a745;
    }
    .badge-danger {
        color: #fff;
        background-color: #dc3545;
    }
</style>

<div class="wrap custom-ontosoccer-contact-us-div">
    <h2 style="margin-bottom: 1rem;"><?php _e( 'Testimonial Entries', 'custom-ontosoccer' ); ?></h2>
    <?php if (isset($_GET['delete-success']) && !empty($_GET['delete-success']) && $_GET['delete-success'] == 'true') { ?>
        <div class="notice notice-success is-dismissible">
            <p><?php _e( 'Testimonial has been deleted successfully!', 'custom-ontosoccer' ); ?></p>
        </div>
    <?php } ?>
    <?php if (isset($_GET['update-success']) && !empty($_GET['update-success']) && $_GET['update-success'] == 'true') { ?>
        <div class="notice notice-success is-dismissible">
            <p><?php _e( 'Testimonial has been updated successfully!', 'custom-ontosoccer' ); ?></p>
        </div>
    <?php } ?>
    <table id="example" class="display datatable testimonial-datatable" style="width:100%;">
        <thead>
            <tr>
                <th>Name</th>
                <th>Email Address</th>
                <th>Phone Number</th>
                <th>Featured</th>
                <th>Message</th>
                <th>Date</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($posts as $post) { ?>
                <tr>
                    <td><?php echo get_post_meta( $post->ID, 'display_name', true ); ?></td>
                    <td><?php echo get_post_meta( $post->ID, 'email_address', true ); ?></td>
                    <td><?php echo get_post_meta( $post->ID, 'mobile_number', true ); ?></td>
                    <?php
                        $featured = get_post_meta( $post->ID, 'featured', true );
                        if (isset($featured) && !empty($featured) && $featured == 'true') { ?>
                            <td><span class="badge badge-success">Enabled</span></td>
                        <?php } else { ?>
                            <td><span class="badge badge-danger">Disabled</span></td>
                        <?php }
                    ?>
                    <td><?php echo get_post_meta( $post->ID, 'message', true ); ?></td>
                    <td><?php echo date('d/m/Y', strtotime($post->post_date)); ?></td>
                    <td>
                        <?php if (isset($featured) && !empty($featured) && $featured == 'true') { ?>
                            <form action="<?php echo admin_url('admin-post.php'); ?>" method="POST" style="display: inline-block;">
                                <input type="hidden" name="action" value="update_custom_ontosoccer_testimonial">
                                <input type="hidden" name="post_id" value="<?php echo $post->ID; ?>">
                                <input type="hidden" name="featured" value="false">
                                <button class="button button-primary" type="submit">Disable</button>
                            </form>
                        <?php } else { ?>
                            <form action="<?php echo admin_url('admin-post.php'); ?>" method="POST" style="display: inline-block;">
                                <input type="hidden" name="action" value="update_custom_ontosoccer_testimonial">
                                <input type="hidden" name="post_id" value="<?php echo $post->ID; ?>">
                                <input type="hidden" name="featured" value="true">
                                <button class="button button-primary" type="submit">Enable</button>
                            </form>
                        <?php } ?>
                        <form action="<?php echo admin_url('admin-post.php'); ?>" method="POST" style="display: inline-block;">
                            <input type="hidden" name="action" value="delete_custom_ontosoccer_testimonial">
                            <input type="hidden" name="post_id" value="<?php echo $post->ID; ?>">
                            <button class="button button-primary" type="submit">Delete</button>
                        </form>
                    </td>
                </tr>
            <?php } ?>
        </tbody>
        <tfoot>
            <tr>
                <th>Name</th>
                <th>Email Address</th>
                <th>Phone Number</th>
                <th>Featured</th>
                <th>Message</th>
                <th>Date</th>
                <th>Actions</th>
            </tr>
        </tfoot>
    </table>
</div>
