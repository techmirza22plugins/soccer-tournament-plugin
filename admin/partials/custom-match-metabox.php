<div class="wrap">
    <table class="form-table">
        <tbody id="custom-ontosoccer-match-data">
            <tr>
                <td class="first-column"><label for="first_team"><?php _e( "First Team", "roster-automation" ); ?></label></td>
                <td>
                    <input type="text" name="first_team" id="first_team" class="wd100" required="required" placeholder="Enter First Team" value="<?php echo get_post_meta($post->ID,"first_team",true); ?>" />
                </td>
            </tr>
            <tr>
                <td><label for="second_team"><?php _e( "Second Team", "roster-automation" ); ?></label></td>
                <td>
                    <input type="text" name="second_team" id="second_team" class="wd100" required="required" placeholder="Enter Second Team" value="<?php echo get_post_meta($post->ID,"second_team",true); ?>" />
                </td>
            </tr>
            <tr>
                <td><label for="match_date"><?php _e( "Match Date", "roster-automation" ); ?></label></td>
                <td>
                    <input type="date" name="match_date" id="match_date" class="wd100" required="required" placeholder="Enter Match Date" value="<?php echo get_post_meta($post->ID,"match_date",true); ?>" />
                </td>
            </tr>
            <tr>
                <td><label for="match_time"><?php _e( "Match Time", "roster-automation" ); ?></label></td>
                <td>
                    <input type="time" name="match_time" id="match_time" class="wd100" required="required" placeholder="Enter Match Time" value="<?php echo get_post_meta($post->ID,"match_time",true); ?>" />
                </td>
            </tr>
        </tbody>
    </table>
</div>